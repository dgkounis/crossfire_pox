#!/usr/bin/python

import time
import sys
import argparse
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *

def send_packets(srcIP,srcMAC,dstIPlist,dstMAClist):
    e = Ether(src=srcMAC,dst=dstMAClist)
    eth = [p for p in e]
    i = IP(src=srcIP,dst=dstIPlist)
    ip = [p for p in i]
    tcp = TCP(sport=41791,dport=80)
    #string = "a"*324
    # invalid URL to have 500 bytes packets sent,
    # measured in wireshark
    # 4 kbps
    string = "a"*430
    # 8 kbps
    #string = "a"*930
    wget = "GET /"+string+" HTTP/1.1\r\n"
    packets = [eth[i]/ip[i]/tcp/wget for i in range(len(eth))]
    #print "packets:", packets 
    sendp(packets)

parser = argparse.ArgumentParser()
parser.add_argument('--list', nargs='+')
args = parser.parse_args()
#print "args:", args.list

srcIP = args.list[0]
srcMAC = args.list[1]
dstIPlist = args.list[2::2]
dstMAClist = args.list[3::2]

print srcIP
print srcMAC
print dstIPlist
print dstMAClist

while True:
    send_packets(srcIP,srcMAC,dstIPlist,dstMAClist)
    time.sleep(1.0)
