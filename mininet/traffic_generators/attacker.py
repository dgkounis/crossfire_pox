#!/usr/bin/python

from mininet.net import Mininet
from threading import Timer
from itertools import groupby
from math import ceil
from collections import deque
import subprocess
import random
import time
import datetime
import signal
import os
import re
import json
import Queue
import logging

logging.basicConfig(filename='attacker.log',filemode='w',level=logging.INFO)
q = None
attack_initial_delay = 30.0
# Let the traceroutes run every 
# more than 10 or 15 seconds so as
# the attacker receives the updated
# link-map (in case of potential reroutings,
# routing instabilities, etc.) 
traceroute_interval = 30.0
#http_request_bw = 8
http_request_bw = 4
initialization = True
#stop_attack_thread = False
bots = []
bots_ip = []
decoy_servers = []
target_servers = []
target_server_ip_list = []
bots_decoy_servers_paths = {}
tr_bots_decoy_servers_paths = {}
bots_target_servers_paths = {}
tr_bots_target_servers_paths = {}
bot_popen = {}
link_capacity = {}
bot_assigned_decoy_servers = {}
initial_assignment = {}
attack_setup_file = None
bot_assignments_file = None
tracerouting_bots = []
allowed_bots = {}
allowed_servers = {}
allowed_tracerouting_bots = {}
target_link_to_tracerouting_bots = {}

#cmd = "traceroute -n 10.0.2.15 | grep -o '[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]*[0-9]*[0-9]' | uniq"
#cmd1 = "traceroute -n 10.0.2.15"
#cmd2 = "grep -o '[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]*[0-9]*[0-9]'"
#cmd3 = "uniq"


def get_traceroute_cmd(ip):
    return "traceroute -w 2.5 -n "+str(ip)
    #return "traceroute -w 2.5 -n "+str(ip)+" | grep -o '[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]*[0-9]*[0-9]' | uniq"

def send_traceroutes():
    global bots_decoy_servers_paths, bots_target_servers_paths, initialization
    global attack_setup_file
    # match a single IP address
    RE_IP = r'\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b'
    tr_regex = re.compile(RE_IP)

    servers = decoy_servers + target_servers
    # all bots perform traceroutes every 30 second
    start = time.time()
    traceroute_time = time.time()
    min_traceroute_time = 40.0
    max_traceroute_time = 0.0
    popens = {}
    target_popens = {}
    decoy_popens = {}
    for i, tracerouting_bot in enumerate(tracerouting_bots):
        hosting_entry_sw = allowed_tracerouting_bots[str(i+1)][0]
        for server in allowed_servers[str(hosting_entry_sw)]:
    	#p1 = subprocess.Popen(cmd1, shell=True, stdout=subprocess.PIPE)
    	    #p2 = subprocess.Popen(cmd2, shell=True, stdin=p1.stdout, stdout=subprocess.PIPE)
    	        #p3 = subprocess.Popen(cmd3, shell=True, stdin=p2.stdout, stdout=subprocess.PIPE)
    	        #popens[(bot,ip)] = p2
    	        #popens[(bot,ip)] = p3
            cmd_string = get_traceroute_cmd(server.IP())
            #print "bot IP:", bot.IP()
            #print "traceroute cmd:", cmd
            traceroute_successful = False
            while not traceroute_successful:
                ###################
                start_traceroute = time.time()
	        traceroute_result = tracerouting_bot.cmd(cmd_string)
	        #popens[(tracerouting_bot,server)] = tracerouting_bot.cmd(cmd_string)
    	        #popens[(bot,server)] = bot.popen(cmd, shell=True)
    	        #popens[(bot,server)] = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
                print "traceroute result:", traceroute_result
                logging.info("traceroute result: "+str(traceroute_result))
                traceroute_successful = True
                dest_ip = str(server.IP())
                dest_hop = 50
                traceroute = ''
                for line in traceroute_result.splitlines()[1:]:
                    # if traceroute result does not have 
                    # more than two arguments
                    if not line.strip().split()[:2]:
                        print 'MALFORMED TRACEROUTE'
                        logging.info('MALFORMED TRACEROUTE')
                        traceroute_successful = False
                        break
                    hop, ip = line.strip().split()[:2]
                    # if the 1st argument is not the hop number
                    if not hop.isdigit():
                        print "Hop is not digit !!!"
                        logging.info("Hop is not digit !!!")
                        traceroute_successful = False
                        break
                    # 1 dst_ip
                    if hop == '1' and ip == dest_ip:
                        print 'DST_IP == HOP_IP'
                        traceroute_successful = False
                        break
                    extracted_ip_re = tr_regex.search(ip)
                    # if the 2nd argument has not an IP address format
                    if not extracted_ip_re:
                        print 'UNDEFINED IP'
                        logging.info('UNDEFINED IP')
                        traceroute_successful = False
                        break
                    else:
                        # if the 2nd argument has an IP address format
                        extracted_ip = extracted_ip_re.group()
                        # to avoid having false traceroutes, i.e.,
                        # the destIP received before the last hop
                        if extracted_ip == dest_ip:
                            dest_hop = int(hop)
                        if dest_hop != 50 and int(hop) > dest_hop: 
                            print 'DEST IP ALREADY RECEIVED - NOT LAST HOP'
                            logging.info('DEST IP ALREADY RECEIVED - NOT LAST HOP')
                            traceroute_successful = False
                            break
                        else:
                            traceroute = traceroute + extracted_ip +'\n'
            popens[(tracerouting_bot,server)] = traceroute
            if server.IP() in target_server_ip_list:
                target_popens[(tracerouting_bot,server)] = popens[(tracerouting_bot,server)]
                if initialization == True:
                    tr_bots_target_servers_paths[(tracerouting_bot,server)] = []
                    for entry_sw in allowed_tracerouting_bots[str(i+1)]:
                        for bot in allowed_bots[str(entry_sw)]:
                            bots_target_servers_paths[(bot,server)] = []
            else:   
                decoy_popens[(tracerouting_bot,server)] = popens[(tracerouting_bot,server)]
                if initialization == True:
                    tr_bots_decoy_servers_paths[(tracerouting_bot,server)] = []
                    for entry_sw in allowed_tracerouting_bots[str(i+1)]:
                        for bot in allowed_bots[str(entry_sw)]:
                            bots_decoy_servers_paths[(bot,server)] = []
            ##################
            if time.time() - start_traceroute > max_traceroute_time:
                max_traceroute_time = time.time() - start_traceroute
            if time.time() - start_traceroute < min_traceroute_time:
                min_traceroute_time = time.time() - start_traceroute
            #################
    if initialization == True:
        initialization = False
    #print "popens:", popens
    # wait for all traceroutes(subprocesses) to finish
    #while True:
    #    popen_status = [popen.poll() for popen in popens.itervalues()]
    #    if all([popen is not None for popen in popen_status]):
    #        break
    print "Tracerouting time:", time.time() - start
    logging.info("Tracerouting time: "+str(time.time() - start))
    #attack_setup_file.write('MIN_TRACEROUTE_TIME: '+str(min_traceroute_time)+'\n')
    #attack_setup_file.write('MAX_TRACEROUTE_TIME: '+str(max_traceroute_time)+'\n')
    #attack_setup_file.write('TRACEROUTES_GATHERED: '+str(time.time())+'\n')
    #attack_setup_file.flush()
    return (target_popens, decoy_popens)


def process_traceroutes(traceroutes):
    global bots_decoy_servers_paths, bots_target_servers_paths
    global tr_bots_decoy_servers_paths, tr_bots_target_servers_paths
    global attack_setup_file

    #attack_setup_file.write('PROCESS_TRACEROUTES: '+str(time.time())+'\n')
    #attack_setup_file.flush()
    logging.info('PROCESS_TRACEROUTES')
    rerouted = False
    target_popens, decoy_popens = traceroutes
    # check if traceroutes have changed
    target_paths = []
    tr_target_paths = {}
    for key, popen in target_popens.iteritems():
        tracerouting_bot, server = key
        tracerouting_bot_index = tracerouting_bots.index(tracerouting_bot)
        for i, entry_sw in enumerate(allowed_tracerouting_bots[str(tracerouting_bot_index+1)]):
            #print "i:", i, "entry_sw:", entry_sw
            #entry_sw = allowed_tracerouting_bots[str(tracerouting_bot_index+1)][i]
            #hop_list = popen.splitlines()[1:]
            hop_list = popen.splitlines()
            #hop_list = map(lambda x: x.rstrip(), popen.readlines())[1:]
            if i != 0: # hosting entry SW
                hop_list[0] = '10.0.1.'+str(entry_sw)
            else:
                tr_target_paths[key] = zip(hop_list[::1], hop_list[1::1])
            target_paths = zip(hop_list[::1], hop_list[1::1])
        #print "Popen output:", popen.communicate()[0]
        #new_out = [item.rstrip() for item in out]
        #traceroutes[key] = map(lambda x: x.rstrip(), popen.stdout.readlines())
        # list of hops 
        #hop_list = map(lambda x: x.rstrip(), popen.stdout.readlines())[1:]
            # list of links, i.e. list of hop pair tuples, 
            # e.g. [(hop1,hop2),(hop2,hop3),...]  
            #print "tr_target_paths["+str(key)+"]:", tr_target_paths[key]
            #print "tr_bots_target_servers_paths["+str(key)+"]:", tr_bots_target_servers_paths[key]
            logging.info("tr_bots_target_servers_paths["+str(key)+"]: "+str(tr_bots_target_servers_paths[key]))
            if tr_bots_target_servers_paths[key] != tr_target_paths[key]:
                for bot in allowed_bots[str(entry_sw)]:
                    bots_target_servers_paths[(bot,server)] = target_paths
                rerouted = True
                print "process_traceroutes() - Target Popens: rerouted==True"
                logging.info("process_traceroutes() - Target Popens: rerouted==True")
        tr_bots_target_servers_paths[key] = tr_target_paths[key]
    print "bots_target_servers_paths:", bots_target_servers_paths

    decoy_paths = []
    tr_decoy_paths = {}
    for key, popen in decoy_popens.iteritems():
        tracerouting_bot, server = key
        tracerouting_bot_index = tracerouting_bots.index(tracerouting_bot)
        for i, entry_sw in enumerate(allowed_tracerouting_bots[str(tracerouting_bot_index+1)]):
            #print "i:", i, "entry_sw:", entry_sw
            #entry_sw = allowed_tracerouting_bots[str(tracerouting_bot_index+1)][i]
            #print "decoy_popen:", popen
            #hop_list = popen.splitlines()[1:]
            hop_list = popen.splitlines()
            #hop_list = map(lambda x: x.rstrip(), popen.readlines())[1:]
            if i != 0: # hosting entry SW
                hop_list[0] = '10.0.1.'+str(entry_sw)
            else:
                tr_decoy_paths[key] = zip(hop_list[::1], hop_list[1::1])
            decoy_paths = zip(hop_list[::1], hop_list[1::1]) 
        #new_out = [item.rstrip() for item in out]
        #traceroutes[key] = map(lambda x: x.rstrip(), popen.stdout.readlines())
        # list of hops 
        #hop_list = map(lambda x: x.rstrip(), popen.stdout.readlines())[1:]
        # list of links, i.e. list of hop pair tuples, 
        # e.g. [(hop1,hop2),(hop2,hop3),...]  
            #print "tr_decoy_paths["+str(key)+"]:", tr_decoy_paths[key]
            #print "tr_bots_decoy_servers_paths["+str(key)+"]:", tr_bots_decoy_servers_paths[key]
            logging.info("tr_bots_decoy_servers_paths["+str(key)+"]: "+str(tr_bots_decoy_servers_paths[key]))
            if tr_bots_decoy_servers_paths[key] != tr_decoy_paths[key]:
                for bot in allowed_bots[str(entry_sw)]:
                    bots_decoy_servers_paths[(bot,server)] = decoy_paths
                rerouted = True
                print "process_traceroutes() - Decoy Popens: rerouted==True"
                logging.info("process_traceroutes() - Decoy Popens: rerouted==True")
        tr_bots_decoy_servers_paths[key] = tr_decoy_paths[key]
    #print "bots_decoy_servers_paths:", bots_decoy_servers_paths
        
    #attack_setup_file.write('LINK_MAP_CONSTRUCTED: '+str(time.time())+'\n')
    #attack_setup_file.flush()
    logging.info('LINK MAP CONSTRUCTED')
    if rerouted == True:
        #attack_setup_file.write('LINK_MAP_CHANGED: '+str(time.time())+'\n')
        #attack_setup_file.flush()
        logging.info('LINK MAP CHANGED')
    return (rerouted, bots_target_servers_paths, bots_decoy_servers_paths)


def find_target_links(bots_target_servers_paths, bots_decoy_servers_paths):
    #global attack_setup_file
    global target_link_to_tracerouting_bots

    start = time.time()
    decoy_links = [] 
    for links_list in bots_decoy_servers_paths.itervalues():
        decoy_links.extend(links_list)

    target_links = [] 
    target_links_and_decoy_freqs = []
    target_links_to_decoy_servers = {}
    while len(target_links) != 4:
        total_links_list = []
        # The total_links_list
        # should contain unique paths
        # (not paths including already 
        # considered target links) 
        for links_list in bots_target_servers_paths.itervalues():
            if target_links:
                unique_links_found = True
                for target_link in target_links:
                    if target_link in links_list:
                        unique_links_found = False
                if unique_links_found:
                    #print "links_list:", links_list
                    total_links_list = total_links_list + links_list
            else:
                total_links_list = total_links_list + links_list
        print "total_links_list:", total_links_list
        logging.info("total_links_list: "+str(total_links_list))
        if total_links_list:
            # Sorted list containing (candidate_target_link, frequency) tuples,
            # frequency is the number of times the links appear in the list
            # total_links_list. List sorted by frequency
            candidate_target_links = [(candidate_target_link, len(list(freq))) for candidate_target_link, freq in groupby(sorted(total_links_list))]
            #print "links:", sorted(exact, key=lambda x: x[1], reverse=True)
            #print "links:", sorted(exact, key=itemgetter(1), reverse=True)
            candidate_target_links = sorted(candidate_target_links, key=lambda x: x[1], reverse=True)
            print "candidate_target_links:", candidate_target_links
            logging.info("candidate_target_links: "+str(candidate_target_links))
            target_link_found = False
            while not target_link_found:
                target_link, freq = candidate_target_links.pop(0)
                if freq == 1:
                    break
                
                unique_decoy_links = list(set(decoy_links))
                if target_link in unique_decoy_links:
                    if target_link not in target_links:
                        target_link_found = True
                        target_link_occurences = decoy_links.count(target_link)
                        target_links_and_decoy_freqs.append((target_link,target_link_occurences))
                        target_links.append(target_link)
            print "target_links_and_decoy_freqs:", target_links_and_decoy_freqs
            logging.info("target_links_and_decoy_freqs:"+str(target_links_and_decoy_freqs))
        else:
            break

    #target_links = sorted(target_links, key=lambda x: x[1], reverse=True)
    for target_link in target_links:
        for bots_decoy_servers, links_list in bots_decoy_servers_paths.iteritems():
            #print "bots_decoy_servers:", bots_decoy_servers
            if target_link in links_list:
                bot_ip, server_ip = bots_decoy_servers    
                if target_link not in target_links_to_decoy_servers:
                    target_links_to_decoy_servers[target_link] = {}
                if server_ip not in target_links_to_decoy_servers[target_link]:
                    target_links_to_decoy_servers[target_link][server_ip] = deque()
                target_links_to_decoy_servers[target_link][server_ip].append(bot_ip)
                ############################
                if bot_ip in tracerouting_bots:
                    if target_link not in target_link_to_tracerouting_bots:
                        target_link_to_tracerouting_bots[target_link] = []
                    if bot_ip not in target_link_to_tracerouting_bots[target_link]:
                        target_link_to_tracerouting_bots[target_link].append(bot_ip)

    print "target_links:", target_links    
    logging.info("target_links: "+str(target_links))
    # WRTITE TARGET LINKS TO FILE
    target_links_file_path = os.path.join(os.getcwd(),"target_links.txt")
    target_links_file = open(target_links_file_path,"w")
    target_links_file.write('\n'.join('%s %s' % x for x in target_links))
    target_links_file.flush()
    target_links_file.close()    
    print "target_links_to_decoy_servers:", target_links_to_decoy_servers 
    logging.info("target_links_to_decoy_servers: "+str(target_links_to_decoy_servers)) 
    print "ATTACKER: find_target_links(): TIME:", time.time() - start
    logging.info("ATTACKER: find_target_links(): TIME: "+str(time.time() - start))
    #attacker_process_traceroutes_time = time.time()
    #attacker_changes_detection_file.write('PROCESS_TRACEROUTES: '+str(attacker_process_traceroutes_time)+'\n')
    #attacker_changes_detection_file.flush()
    #logging.info('PROCESS_TRACEROUTES')
    return target_links_to_decoy_servers


# assignment phase 
def bot_assignment(target_links_to_decoy_servers):
    global bot_assignments_file
    global target_link_to_tracerouting_bots
    bot_assigned_decoy_servers = {} 
    decoy_server_assigned_bots = {}
    for target_link in target_links_to_decoy_servers.iterkeys():
        print "BOT ASSIGNMENT: target_link: ", target_link
        # Dict of decoy_server -> deque of bot_ips 
        decoy_servers_to_bots_dict = target_links_to_decoy_servers[target_link]
        print "decoy_servers_to_bots_dict: ", str(decoy_servers_to_bots_dict)
        logging.info("decoy_servers_to_bots_dict: "+str(decoy_servers_to_bots_dict)) 

        # get the target link capacity (in kbps)
        target_link_capacity = link_capacity[tuple(sorted(target_link))]
        print "target_link_capacity:", str(target_link_capacity)
        logging.info("target_link_capacity: "+str(target_link_capacity))
        # calculate the required number of flows to flood the target link
        required_flows = int(ceil(target_link_capacity / http_request_bw))
        print "required_flows:", str(required_flows)
        logging.info("required_flows: "+str(required_flows))

        # assign all the tracerouting bots to each decoy server
        for decoy_server, bots_deque in decoy_servers_to_bots_dict.iteritems():
            for tr_bot in target_link_to_tracerouting_bots[target_link]:
                if tr_bot in bots_deque:
                    bots_list = list(bots_deque)
                    bots_list.remove(tr_bot)
                    bots_list.append(tr_bot)
                    decoy_servers_to_bots_dict[decoy_server] = deque(bots_list)
                    if decoy_server.IP() not in decoy_server_assigned_bots:
                        decoy_server_assigned_bots[decoy_server.IP()] = []
                    if tr_bot.IP() not in bot_assigned_decoy_servers:
                        bot_assigned_decoy_servers[tr_bot.IP()] = []
                    bot_assigned_decoy_servers[tr_bot.IP()].append(decoy_server.IP())
                    decoy_server_assigned_bots[decoy_server.IP()].append(tr_bot.IP())
                    required_flows = required_flows - 1

        while required_flows > 0:
            unique_assigned_bots = []
            for decoy_server, bots_deque in decoy_servers_to_bots_dict.iteritems():
                print "decoy_server:", decoy_server
                logging.info("decoy_server: "+str(decoy_server))
                #print "bots_deque:", bots_deque
                logging.info("bots_deque: "+str(bots_deque))
                # select the bot in the top of the queue
                bot = bots_deque.popleft()
                # if this bot has already been assigned
                if bot in unique_assigned_bots:
                    # find if there is a unique bot in the queue
                    if list(set(bots_deque)-set(unique_assigned_bots)):
                        # find the unique bot
                        while bot in unique_assigned_bots:
                            # place the selected bot at the end of the queue
                            bots_deque.append(bot)                  
                            # select the bot in the top of the queue
                            bot = bots_deque.popleft()
                        unique_assigned_bots.append(bot) 
                else:
                    unique_assigned_bots.append(bot)
                # place the selected bot at the end of the queue
                bots_deque.append(bot)
                if decoy_server.IP() not in decoy_server_assigned_bots:
                    decoy_server_assigned_bots[decoy_server.IP()] = []
                if bot.IP() not in bot_assigned_decoy_servers:
                    bot_assigned_decoy_servers[bot.IP()] = []
                bot_assigned_decoy_servers[bot.IP()].append(decoy_server.IP())
                decoy_server_assigned_bots[decoy_server.IP()].append(bot.IP())
                required_flows = required_flows - 1
                if required_flows == 0:
                    break
            print "required_flows:", required_flows
            logging.info("required_flows: "+str(required_flows))

    target_link_to_tracerouting_bots = {}
    print "bot_assigned_decoy_servers:", str(bot_assigned_decoy_servers)
    logging.info("bot_assigned_decoy_servers: "+str(bot_assigned_decoy_servers))
    #bot_assignments_file.write('bot_assigned_decoy_servers: '+json.dumps(bot_assigned_decoy_servers)+'\n')
    #bot_assignments_file.write('decoy_server_assigned_bots: '+json.dumps(decoy_server_assigned_bots)+'\n')
    #bot_assignments_file.write('\n')
    #bot_assignments_file.flush()
    return bot_assigned_decoy_servers


def launch_attack(bot_assigned_decoy_servers):
    # launch HTTP GET Requests to the assigned Decoy Servers
    global bot_popen, attacker_attack_times_file, attack_setup_file
    global globalIPtoMACmap
    #global stop_attack_thread
    global q    

    sleep_seconds = 1.0
    for bot in bots:
        #string_url = "a" * 324
        #decoy_servers_number = len(bot_assigned_decoy_servers[bot.IP()])
        #sleep_period = str(sleep_seconds / float(decoy_servers_number))
        #attack_script = 'while true; do'
        bot_MAC = globalIPtoMACmap[bot.IP()]
        scapy_args = bot.IP()+" "+bot_MAC 
        for decoy_server_ip in bot_assigned_decoy_servers[bot.IP()]:
            scapy_args = scapy_args+" "+str(decoy_server_ip)+" "+globalIPtoMACmap[decoy_server_ip]
            #server_port = 80 + int(decoy_server_ip.split('.')[3])
            #url = "http://"+str(decoy_server_ip)+":"+str(server_port)+"/"+string_url+"/"
            #attack_script = attack_script + ' wget --spider -t 1 '+url+' &'
            #attack_script = attack_script + ' wget --spider -t 1 -T 0.1 '+url+' sleep '+sleep_period+' ;'
            #attack_script = attack_script + ' wget --spider '+url
        print "SCAPY ARGS:", scapy_args
        #attack_script = attack_script + ' sleep '+str(sleep_seconds)+' ; done &'
        #attack_script = attack_script + ' sleep '+str(sleep_seconds)+' ; done'
        #attack_script = attack_script + ' done &'
        #print "bot:", bot.name, "sleep_period:", sleep_period, "attack_script:", attack_script
        #bot_popen[bot.name] = bot.popen('/bin/sh', '-c', attack_script, preexec_fn=os.setsid)
        #bot.popen('/bin/sh', '-c', "while true; do sudo python packet_generator.py "+scapy_args+" ; sleep 1.0; done", preexec_fn=os.setsid)
        bot.cmd('nohup ./sender_script.py --list '+scapy_args+' &')
        #print "bot:", bot.name, "PID:", bot_popen[bot.name].pid
    #attacker_attack_times_file.write('START: '+str(datetime.datetime.now())+'\n')
    #attacker_attack_times_file.flush()
    print "ATTACKER: launch_attack():", str(datetime.datetime.now())
    logging.info("ATTACKER: launch_attack(): "+str(datetime.datetime.now()))

    #attack_setup_file.write('ATTACK_LAUNCHED: '+str(time.time())+'\n')
    #attack_setup_file.flush()
    logging.info('PROCESS_TRACEROUTES')

    stop_attack_thread = False
    while True:
        for bot in bots:
            bot.cmd('sleep 1')
            if not q.empty():
                print "Queue Active"
                stop_attack_thread = q.get()
                if stop_attack_thread:
                    print "STOP attack thread"
                    break
        if stop_attack_thread:
            print "STOP attack thread"
            break
    print "ATTACK THREAD STOPPED"
    #stop_attack_thread = False
    #q.put(stop_attack_thread)


def stop_attack():
    global bot_popen, initialization
    global attacker_attack_times_file
    #global stop_attack_thread
    global q

    if not initialization:
        #attacker_attack_times_file.write('STOP: '+str(datetime.datetime.now())+'\n')
        #attacker_attack_times_file.flush()
        print "ATTACKER: stop_attack():", str(datetime.datetime.now())
        logging.info("ATTACKER: stop_attack(): "+str(datetime.datetime.now()))
        stop_attack_thread = True
        q.put(stop_attack_thread)
        f1 = open("attack_traffic_pids.txt","w")
        get_attack_traffic_pids = subprocess.Popen("ps -ef | grep /usr/bin/python | awk '{print $2}'",shell=True,stdout=f1)
        get_attack_traffic_pids.communicate()
        f1.close()
        attack_traffic_pids_file = open("attack_traffic_pids.txt","r")
        for line in attack_traffic_pids_file.readlines():
            print "line:", line.strip()         
            subprocess.Popen("sudo kill -9 "+line.strip(),shell=True)
        attack_traffic_pids_file.close()
        os.remove(os.path.join(os.getcwd(),"attack_traffic_pids.txt"))
        #for bot in bots:
        #    os.killpg(bot_popen[bot.name].pid, signal.SIGTERM)


def timer():
    global bot_assigned_decoy_servers
    #global previous_timer_thread    

    #stop_attack()
    # comment the 2 lines below
    #if not initialization:
    #    launch_attack(bot_assigned_decoy_servers)
    traceroutes = send_traceroutes()
    rerouted, target_servers_paths, decoy_servers_paths = process_traceroutes(traceroutes)
    if rerouted == True:
        print "ATTACKER: ============================"
        logging.info("ATTACKER: ============================")
        print "ATTACKER: timer(): rerouted==TRUE"
        logging.info("ATTACKER: timer(): rerouted==TRUE")
        print "ATTACKER: ============================"
        logging.info("ATTACKER: ============================")
        target_links_to_decoy_servers = find_target_links(target_servers_paths, decoy_servers_paths)
        bot_assigned_decoy_servers = bot_assignment(target_links_to_decoy_servers)
        stop_attack()
        # comment the line below
        launch_attack(bot_assigned_decoy_servers)
    t = Timer(traceroute_interval, timer)
    t.start()
    # uncomment the line below
    #launch_attack(bot_assigned_decoy_servers)

    
def main(link_bw, bots_list, tracerouting_bots_list, decoy_servers_list, target_servers_list, allowed_bots_dict, allowed_servers_dict, allowed_tracerouting_bots_dict, IPtoMACMap):
    global bots, tracerouting_bots, bots_ip, link_capacity, decoy_servers, target_servers
    global allowed_bots, allowed_servers, allowed_tracerouting_bots
    global attack_setup_file, bot_assignments_file
    global attacker_attack_times_file
    global target_server_ip_list
    global globalIPtoMACmap
    global q

    current_path = os.getcwd()
    primary_folder_path = os.path.split(current_path)[0]
    #results_folder_path = os.path.join(primary_folder_path,'results')
    #attacker_attack_times_file = open(results_folder_path+"/attacker_attack_times.txt",'w')
    #attacker_reaction_times_folder_path = os.path.join(results_folder_path,'reaction_times','attacker')
    #attack_setup_file = open(attacker_reaction_times_folder_path+"/attack_setup.txt",'w')
    #bot_assignments_folder_path = os.path.join(results_folder_path,'bot_assignments')
    #bot_assignments_file = open(bot_assignments_folder_path+"/bot_assignments.txt",'w')

    q = Queue.Queue(1)

    bots = bots_list
    tracerouting_bots = tracerouting_bots_list    
    bots_ip = [bot.IP() for bot in bots_list]
    print "bots:", bots_ip
    link_capacity = link_bw
    print "link_capacity", link_capacity
    decoy_servers = decoy_servers_list
    decoy_server_ip_list = [decoy_server.IP() for decoy_server in decoy_servers_list]
    print "decoy_servers:", decoy_servers 
    print "decoy_server_ip_list:", decoy_server_ip_list 
    target_servers = target_servers_list
    target_server_ip_list = [target_server.IP() for target_server in target_servers_list]
    print "target_servers:", target_servers 
    print "target_server_ip_list:", target_server_ip_list 

    allowed_bots = allowed_bots_dict
    allowed_servers = allowed_servers_dict
    print "allowed_servers:", allowed_servers
    allowed_tracerouting_bots = allowed_tracerouting_bots_dict
    globalIPtoMACmap = IPtoMACMap

    # start the attack after some time
    # to establish that the legitimate 
    # users initially send traffic that 
    # is not affected by the attack
    # traffic (wgets and traceroutes)
    Timer(attack_initial_delay, timer).start()
