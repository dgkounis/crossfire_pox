#!/usr/bin/python

from mininet.net import Mininet
from threading import Timer
import subprocess
import random
import time
import datetime
import signal
import os

traceroute_interval = 30.0
avg_arrival_time = 1/2.0
avg_send_rate = 1/8.0
total_requests = 5

user_decoy_popen = {}
user_target_popen = {}
user_assigned_decoy_servers = {}
user_assigned_target_servers = {}
allow_users = {}
allow_servers = {}
user_file_descr = {} 
users = []
users_ip = []
decoy_servs = [] 
target_servs = []


# assignment phase
def user_assignment(): 
    global user_assigned_decoy_servers, user_assigned_target_servers
    
    for entry_sw, servers in allow_servers.iteritems():
        for user in allow_users[str(entry_sw)]:
            decoy_server = random.choice(servers)
            user_assigned_decoy_servers[user.IP()] = [decoy_server.IP()]
            target_server = random.choice(target_servs)
            user_assigned_target_servers[user.IP()] = [target_server.IP()]
    print "user_assigned_decoy_servers:", user_assigned_decoy_servers 
    print "user_assigned_target_servers:", user_assigned_target_servers


    #for user_ip in users_ip:
    #    decoy_server = random.choice(decoy_servers)
    #    user_assigned_decoy_servers[user_ip] = [decoy_server.IP()]
    #    target_server = random.choice(target_servers)
    #    user_assigned_target_servers[user_ip] = [target_server.IP()]
    #print "user_assigned_decoy_servers:", user_assigned_decoy_servers 
    #print "user_assigned_target_servers:", user_assigned_target_servers


def launch_legitimate_traffic(user):
    global user_decoy_popen, user_target_popen

    decoy_expo_send_times = [random.expovariate(avg_send_rate) for r in xrange(total_requests)]
    target_expo_send_times = [random.expovariate(avg_send_rate) for r in xrange(total_requests)]
   
    #decoy_script = 'while true; do '
    #for decoy_server_ip in user_assigned_decoy_servers[user.IP()]:
    #    server_port = 80 + int(decoy_server_ip.split('.')[3])
    #    url = "http://"+str(decoy_server_ip)+":"+str(server_port)+"/ ;"
    #    for send_time in decoy_expo_send_times:
    #        decoy_script = decoy_script + 'wget -T 1 '+url+' sleep '+str(send_time)+' ; '
    #decoy_script = decoy_script + 'done &'
    #user_decoy_popen[user.name] = user.popen('/bin/sh', '-c', decoy_script, preexec_fn=os.setsid)

    target_script = 'while true; do '
    for target_server_ip in user_assigned_target_servers[user.IP()]:        
        server_port = 80 + int(target_server_ip.split('.')[3])
        url = "http://"+str(target_server_ip)+":"+str(server_port)+"/ ;"
        for send_time in target_expo_send_times:
            target_script = target_script + 'wget -t 1 '+url+' sleep '+str(send_time)+' ; '
    target_script = target_script + 'done &'
    user_target_popen[user.name] = user.popen('/bin/sh', '-c', target_script, preexec_fn=os.setsid, stderr=user_file_descr[user.name])
    print "USER: launch_legitimate_traffic("+str(user.name)+")", str(datetime.datetime.now()), "PID:", str(user_target_popen[user.name].pid)

def stop_legitimate_traffic():
    global user_decoy_popen, user_target_popen

    if user_decoy_popen and user_target_popen:
        print "USER: stop_legitimate_traffic():", str(datetime.datetime.now())
        for user in users:
            os.killpg(user_decoy_popen[user.name].pid, signal.SIGTERM)
            os.killpg(user_target_popen[user.name].pid, signal.SIGTERM)


def timer():

    stop_legitimate_traffic()
    user_expo_arrival_times = [random.expovariate(avg_arrival_time) for r in xrange(len(users))]
    for index, user in enumerate(users):
        Timer(user_expo_arrival_times[index], launch_legitimate_traffic, [user]).start()
    t = Timer(traceroute_interval, timer)
    t.start()

    
def main(users_list, decoy_servers_list, target_servers_list, allowed_users_dict, allowed_servers_dict):
    global users, users_ip, decoy_servs, target_servs, user_file_descr
    global allow_users, allow_servers

    users = users_list
    users_ip = [user.IP() for user in users_list]

    current_path = os.getcwd()
    primary_folder_path = os.path.split(current_path)[0]
    results_folder_path = os.path.join(primary_folder_path,'results')
    user_wget_output_folder_path = os.path.join(results_folder_path,'service_rate','user_wget_output')   
    # user.name -> file_descr
    for user in users_list:
        user_file_descr[user.name] = open(user_wget_output_folder_path+'/'+user.name+'.txt','w') 

    decoy_servs = decoy_servers_list 
    target_servs = target_servers_list
    allow_users = allowed_users_dict
    allow_servers = allowed_servers_dict
    for entry_sw, servers in allow_servers.iteritems():
        allow_servers[entry_sw] = [server for server in servers if server not in target_servs]

    user_assignment()
    timer()
