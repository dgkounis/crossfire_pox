#!/usr/bin/python

import subprocess
import os

###################################
#          CLEAN MININET          #
###################################

subprocess.Popen("sudo mn -c", shell=True).communicate()

###################################
#        DELETE WEB SERVERS       #
###################################

subprocess.Popen("sudo pkill -9 python", shell=True).communicate()

###################################
# DELETE QUEUES FROM OVS SWITHCES #
###################################

f1 = open("qos_ids.txt","w")
f2 = open("queue_ids.txt","w")

get_qos_ids = subprocess.Popen("sudo ovs-vsctl list QoS | grep _uuid", shell=True, stdout=f1)
get_qos_ids.communicate()
get_queue_ids = subprocess.Popen("sudo ovs-vsctl list Queue | grep _uuid", shell=True, stdout=f2)
get_queue_ids.communicate()
f1.close()
f2.close()

qos_ids_file = open("qos_ids.txt","r")
for line in qos_ids_file.readlines():
    subprocess.Popen("sudo ovs-vsctl destroy QoS "+line.split()[2], shell=True)
qos_ids_file.close()

queue_ids_file = open("queue_ids.txt","r")
for line in queue_ids_file.readlines():
    subprocess.Popen("sudo ovs-vsctl destroy Queue "+line.split()[2], shell=True)
queue_ids_file.close()

os.remove(os.path.join(os.getcwd(),"qos_ids.txt"))
os.remove(os.path.join(os.getcwd(),"queue_ids.txt"))

###################################
#  DELETE REMAINING WGET SCRIPTS  #
###################################

f1 = open("wget_pids.txt","w")
get_wget_pids = subprocess.Popen("ps -ef | grep /bin/sh | awk '{print $2}'", shell=True, stdout=f1)
get_wget_pids.communicate()
f1.close()

wget_pids_file = open("wget_pids.txt","r")
for line in wget_pids_file.readlines():
    subprocess.Popen("sudo kill -9 "+line.strip(), shell=True)
wget_pids_file.close()

os.remove(os.path.join(os.getcwd(),"wget_pids.txt"))

##################################
#    DELETE TARGET LINKS FILE    #  
##################################

subprocess.Popen("sudo rm target_links.txt", shell=True).communicate()
