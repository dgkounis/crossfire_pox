#!/usr/bin/python

from mininet.net import Mininet 
from mininet.topo import Topo 
from mininet.cli import CLI 
from mininet.link import TCLink 
from mininet.log import setLogLevel
from mininet.node import RemoteController 
from mininet.util import dumpNodeConnections

from traffic_generators import attacker, user

import time
import copy

bots = []
users = []
decoy_servers = []
target_servers = []
tracerouting_bots = []
allowed_servers = {}
allowed_bots = {}
allowed_users = {}
allowed_hosts = {}
allowed_tracerouting_bots = {}
entry_switches_number = 4

class ReroutingTopo(Topo):

    def __init__(self, **opts):
        # Initialize topology and default options
        Topo.__init__(self, **opts)

        switches_num = 15
        switches = []
        for i in range(switches_num):
            switches.append(self.addSwitch('s%s' %(i+1)))
        
        bot_num = 12
        bots = []
        for i in range(bot_num):
            bots.append(self.addHost('b%s' %(i+1)))

        user_num = 8
        users = []
        for i in range(user_num):
            users.append(self.addHost('u%s' %(i+1)))
        
        decoy_server_num = 12
        decoy_servers = []
        for i in range(decoy_server_num):
            decoy_servers.append(self.addHost('d%s' %(i+1)))
        
        target_server_num = 1
        target_servers = []
        for i in range(target_server_num):
            target_servers.append(self.addHost('t%s' %(i+1)))


        ##### Add links to the Enterprise Network
        # s1-s5
        self.addLink(switches[0],switches[4],bw=1)
        # s1-s2
        self.addLink(switches[0],switches[1],bw=1)
        ## s1-s6
        #self.addLink(switches[0],switches[5],bw=1)
        # s2-s5
        self.addLink(switches[1],switches[4],bw=1)
        # s2-s3
        self.addLink(switches[1],switches[2],bw=1)
        ## s2-s6
        #self.addLink(switches[1],switches[5],bw=1) 
        # s3-s6 
        self.addLink(switches[2],switches[5],bw=1)
        # s3-s4 
        self.addLink(switches[2],switches[3],bw=1)
        ## s3-s5 
        #self.addLink(switches[2],switches[4],bw=1)
        # s4-s6
        self.addLink(switches[3],switches[5],bw=1)
        ## s4-s5
        #self.addLink(switches[3],switches[4],bw=1)

        # s5-s7 
        self.addLink(switches[4],switches[6],bw=1)
        # s7-s11 
        self.addLink(switches[6],switches[10],bw=1)
        # s11-s12
        self.addLink(switches[10],switches[11],bw=1)
        # s11-s8
        self.addLink(switches[10],switches[7],bw=1)
        # s5-s8, 400 kbps
        self.addLink(switches[4],switches[7],bw=0.2) 
        # s8-s12 
        self.addLink(switches[7],switches[11],bw=1)

        # s6-s10
        self.addLink(switches[5],switches[9],bw=1) 
        # s10-s13 
        self.addLink(switches[9],switches[12],bw=1)
        # s13-s14
        self.addLink(switches[12],switches[13],bw=1)
        # s13-s9
        self.addLink(switches[12],switches[8],bw=1)
        # s6-s9, 400 kbps
        self.addLink(switches[5],switches[8],bw=0.2)
        # s9-s14
        self.addLink(switches[8],switches[13],bw=1) 

        # s12-s15 
        self.addLink(switches[11],switches[14],bw=1)
        # s14-s15
        self.addLink(switches[13],switches[14],bw=1)

        ##### Attach target servers
        # s15-targetServer1
        self.addLink(switches[14],target_servers[0])

        ##### Attach decoy servers
        # s12-decoyServer1
        self.addLink(switches[11],decoy_servers[0])  
	# s12-decoyServer2
        self.addLink(switches[11],decoy_servers[1])
	# s12-decoyServer3
        self.addLink(switches[11],decoy_servers[2])
	# s8-decoyServer4
        self.addLink(switches[7],decoy_servers[3])
	# s8-decoyServer5
        self.addLink(switches[7],decoy_servers[4])
	# s8-decoyServer6
        self.addLink(switches[7],decoy_servers[5])
	# s14-decoyServer7
        self.addLink(switches[13],decoy_servers[6])
	# s14-decoyServer8
        self.addLink(switches[13],decoy_servers[7])
	# s14-decoyServer9
        self.addLink(switches[13],decoy_servers[8])
	# s9-decoyServer10
        self.addLink(switches[8],decoy_servers[9])
	# s9-decoyServer11
        self.addLink(switches[8],decoy_servers[10])
	# s9-decoyServer12
        self.addLink(switches[8],decoy_servers[11])

        ###### Attach bots to entry switches
        # bot1-3 - s1
        # bot4-6 - s2
        # bot7-9 - s3
        # bot10-12 - s4
        first_entry_sw = range(3)
        second_entry_sw = range(3,6)
        third_entry_sw = range(6,9)
        forth_entry_sw = range(9,12)
        for i in range(len(bots)):
            if i in first_entry_sw:
                self.addLink(bots[i],switches[0],delay='100ms') 
            elif i in second_entry_sw:
                self.addLink(bots[i],switches[1],delay='100ms')
            elif i in third_entry_sw:
                self.addLink(bots[i],switches[2],delay='100ms')
            elif i in forth_entry_sw:
                self.addLink(bots[i],switches[3],delay='100ms')

        ##### Attach users to entry switches
        # user1-2 - s1
        # user3-4 - s2
        # user5-6 - s3
        # user7-8 - s4
        first_entry_sw = range(2)
        second_entry_sw = range(2,4)
        third_entry_sw = range(4,6)
        forth_entry_sw = range(6,8)
        for i in range(len(users)):
            if i in first_entry_sw:
                self.addLink(users[i],switches[0],delay='100ms') 
            elif i in second_entry_sw:
                self.addLink(users[i],switches[1],delay='100ms')
            elif i in third_entry_sw:
                self.addLink(users[i],switches[2],delay='100ms')
            elif i in forth_entry_sw:
                self.addLink(users[i],switches[3],delay='100ms')


def test():
    global bots, users, decoy_servers, target_servers 
    global tracerouting_bots, allowed_servers

    topo = ReroutingTopo()
    file = open('linkBW.txt', 'w')
    links = topo.links()
    print "links:", links
    link_capacity = {}
    port_capacity = {}
    for link in links:
        print link, topo.linkInfo(link[0], link[1])
        if topo.linkInfo(link[0], link[1]) and topo.linkInfo(link[0], link[1]).keys() == ['bw']:
            print "port:", topo.port(link[0], link[1])
            file.writelines(['sw: ', str(link[0]), ' sw: ', str(link[1]), ' bw: ', str(topo.linkInfo(link[0], link[1])['bw']), '\n'])
            # link_capacity in kbps
            link_capacity[('10.0.1.'+link[0].split('s')[1],'10.0.1.'+link[1].split('s')[1])] = topo.linkInfo(link[0],link[1])['bw'] * 1000.0
            if link[0] not in port_capacity:
                port_capacity[link[0]] = []
            if link[1] not in port_capacity:
                port_capacity[link[1]] = []
            port_capacity[link[0]].append((topo.port(link[0], link[1])[0],topo.linkInfo(link[0], link[1])['bw']*1000000.0))
            port_capacity[link[1]].append((topo.port(link[0], link[1])[1],topo.linkInfo(link[0], link[1])['bw']*1000000.0))
    print "port_capacity:", port_capacity
    file.close()
    net = Mininet(topo=topo, autoSetMacs=True, link=TCLink, build=False)
    net.build()
    c0 = net.addController('c0', controller=RemoteController, port=6633)
    c0.start()
    # install 2 queues at every port of the switch.
    # The 1st is the default (queue_id=0) and has the
    # nominal bandwidth and the 2nd is the queue needed
    # for rate-limiting. This has half of the nominal capacity
    for switch in net.switches:
        switch.start([c0])
        sw_name = switch.name
        for port, nominal_bw in port_capacity[sw_name]:
            print sw_name, port, nominal_bw
            cmd_string = "sudo ovs-vsctl -- set port "+sw_name+"-eth"+str(port)+" qos=@newqos -- " +\
                "--id=@newqos create qos type=linux-htb " +\
                "other-config:max-rate="+str(nominal_bw)+" " +\
                "queues=0=@q0,1=@q1 -- " +\
                "--id=@q0 create queue other-config:max-rate="+str(nominal_bw)+" -- " +\
                "--id=@q1 create queue other-config:max-rate="+str(nominal_bw/2.0)
            print cmd_string
            switch.cmd(cmd_string)

            #cmd_string = "sudo ovs-vsctl -- set port "+sw_name+"-eth"+str(port)+" qos=@newqos -- " +\
            #    "--id=@newqos create qos type=linux-htb " +\
            #    "other-config:max-rate="+str(nominal_bw)+" " +\
            #    "queues=0=@q0,1=@q1 -- " +\
            #    "--id=@q0 create queue other-config:max-rate="+str(nominal_bw)+" -- " +\
            #    "--id=@q1 create queue other-config:max-rate="+str(nominal_bw/2.0)    

    print "Printing host and switch info"
    dumpNodeConnections(net.hosts)
    dumpNodeConnections(net.switches)

    #CLI(net)
    time.sleep(1.0)

    print "Defining bots, users, decoy and target servers"
    globalIpToMacMap = {}
    for host in net.hosts:
        ifconfig_first_line = host.cmd('ifconfig '+str(host.name)+'-eth0').split('\n')[0].strip().split()
        hw_index = ifconfig_first_line.index('HWaddr')
        globalIpToMacMap[host.IP()] = ifconfig_first_line[(hw_index + 1)]
        if host.name.startswith('b'):
            bots.append(host)               
        elif host.name.startswith('u'):
            users.append(host)
        elif host.name.startswith('d'):
            decoy_servers.append(host)
            host.cmd('sudo /sbin/iptables -A INPUT -p tcp --destination-port 80 -j DROP')
        elif host.name.startswith('t'):
            target_servers.append(host)
    # Let the tracerouting bots be bot1 and bot7
    tracerouting_bots.append(bots[0])
    tracerouting_bots.append(bots[6])

    print "Print the IP to MAC mappings for all hosts:"
    print globalIpToMacMap

    print "Launching the HTTP service of the servers"
    #servers = decoy_servers + target_servers
    #server_port = 80
    #for server in servers:
    for server in target_servers:
        server_port = 80 + int(server.IP().split('.')[3])
        server.cmd('python -m SimpleHTTPServer '+str(server_port)+' &')
        #server.cmd('sudo python http_server.py '+str(server_port)+' &')

    define_allowed_servers()

    print "Warm UP"
    print "Pinging the servers"
    for i in range(entry_switches_number):
        for host in allowed_hosts[str(i+1)]:
            print "host: ", host, "server:", 
            for server in allowed_servers[str(i+1)]:    
                host.popen('/bin/sh', '-c', 'ping -c5 '+ str(server.IP()) +' &')
                print server,
            print
 
    time.sleep(15.0)

    #CLI(net)

    print "Launch legitimate users"
    allowed_servers_copy = {}
    for key, value in allowed_servers.iteritems():
        allowed_servers_copy[key] = value
    user.main(users, decoy_servers, target_servers, allowed_users, allowed_servers_copy)
    print "Launch attack"
    attacker_multiple_traceroutes.main(link_capacity, bots, tracerouting_bots, decoy_servers, target_servers, allowed_bots, allowed_servers, allowed_tracerouting_bots, globalIpToMacMap)
    #attacker_multiple_traceroutes.main(link_capacity, bots, tracerouting_bots, decoy_servers, target_servers, allowed_bots, allowed_servers, allowed_tracerouting_bots)
    #attacker.main(link_capacity, bots, tracerouting_bots, decoy_servers, target_servers, allowed_bots, allowed_servers, allowed_tracerouting_bots)
   
    CLI(net)

    print "Killing HTTP servers"
    for server in servers:
        server.cmd('kill %python')

    print "Stopping test"
    net.stop()



def define_allowed_servers():

    global allowed_servers, allowed_bots, allowed_users, allowed_hosts
    global allowed_tracerouting_bots

    for i in range(entry_switches_number):
        if i == 0:
            # Decoy Servers 1-6
            allowed_servers[str(i+1)] = [decoy_server for decoy_server in decoy_servers[:6]]
            # Bots 1-3
            allowed_bots[str(i+1)] = [bot for bot in bots[:3]]
            # User 1-2
            allowed_users[str(i+1)] = [user for user in users[:2]]
        elif i == 1:
            # Decoy Servers 1-6
            allowed_servers[str(i+1)] = [decoy_server for decoy_server in decoy_servers[:6]]
            # Bots 4-6
            allowed_bots[str(i+1)] = [bot for bot in bots[3:6]]
            # User 3-4
            allowed_users[str(i+1)] = [user for user in users[2:4]]
        elif i == 2:
            # Decoy Servers 7-12
            allowed_servers[str(i+1)] = [decoy_server for decoy_server in decoy_servers[6:]]
            # Bots 7-9
            allowed_bots[str(i+1)] = [bot for bot in bots[6:9]]
            # User 5-6
            allowed_users[str(i+1)] = [user for user in users[4:6]]
        elif i == 3:
            # Decoy Servers 7-12
            allowed_servers[str(i+1)] = [decoy_server for decoy_server in decoy_servers[6:]]
            # Bots 10-12
            allowed_bots[str(i+1)] = [bot for bot in bots[9:12]]
            # User 7-8
            allowed_users[str(i+1)] = [user for user in users[6:8]]
        allowed_hosts[str(i+1)] = allowed_bots[str(i+1)] + allowed_users[str(i+1)]
        allowed_servers[str(i+1)].extend(target_servers)

    # assign tracerouting bots to entry switches
    # tr_bot 1 -> 1st and 2nd entry SW
    # tr_bot 2 -> 3rd and 4th entry SW
    # the first item of the entry SW list for each tr_bot
    # is the SW that the tr_bot is connected 
    for i in range(len(tracerouting_bots)):
        if (i+1) == 1:
            allowed_tracerouting_bots[str(i+1)] = [1,2]
        elif (i+1) == 2:
            allowed_tracerouting_bots[str(i+1)] = [3,4]        

    print "allowed_servers:", allowed_servers 
    print "allowed_bots:", allowed_bots 
    print "allowed_users:", allowed_users 
    print "allowed_tracerouting_bots:", allowed_tracerouting_bots    
    


if __name__ == '__main__':
    setLogLevel('info')
    test()
