from pox.core import core
from pox.openflow import *
import pox.openflow.libopenflow_01 as of
from pox.openflow.discovery import *
import pox.lib.packet as pkt
from pox.lib.packet.ipv4 import ipv4
from crossfire_pox.icmp import icmp, exceed, TYPE_TIME_EXCEED
from pox.lib.packet.arp import arp
from pox.lib.addresses import EthAddr, IPAddr
from pox.lib.util import dpid_to_str
log = core.getLogger()
from pox.lib.revent import *
from pox.lib.recoco import Timer
import time
import traceback
import sys
import copy
import random
import os

MAX_PHYS_PORTS = 0xFF00

class ReroutingMonitoring(Event):
    """ Fired to monitor rerouted paths """
    #def __init__(self, con, msg):
    def __init__(self, msg):
        Event.__init__(self)
        #self.con = con
        self.msg = msg


class DijkstraRouting (EventMixin):
    
    #_core_name = 'dijkstra_routing'
    _neededComponents = set(['openflow_discovery', 'link_monitoring'])
    _eventMixin_events = set([ReroutingMonitoring])
    
    def __init__(self):
        log.info('DijkstraRouting __init__')
        super(EventMixin, self).__init__()
        self.switches = {}     #key=dpid, value = SwitchWithPaths instance
        self.sw_sw_ports = {}  #key = (dpid1,dpid2), value = outport of dpid1
        self.phys_adjs = {}    #key = dpid, value = list of neighbors  
        self.dijkstra = Dijkstra(self.switches)
        self.arpmap = {}      #key=IP, value = (mac,dpid,port)
        self._paths_computed = False #boolean for path computation verification
        self.flow_rules_idle_timeout = 3.0
        self.rate_limit_threshold = 3
        self.rate_limited_flows = []
        self.traceroute_dst_ports = [port for port in range(33434, 33535)]
        core.listen_to_dependencies(self, self._neededComponents)
        #self.open_results_files()
    
    def _all_dependencies_met (self):
        # when all the depending components are registered
        self.listenTo(core)
        self.listenTo(core.openflow)

    def _handle_ConnectionUp(self, event):
        log.info('ConnectionUp')
        if event.dpid not in self.switches.keys():
            connected_switch = SwitchWithPaths()
            self.switches[event.dpid] = connected_switch
            if event.dpid not in self.phys_adjs.keys():
                self.phys_adjs[event.dpid] = []
        self.switches[event.dpid].connect(event.connection)
        msg_ARP = of.ofp_flow_mod()
        msg_IP  = of.ofp_flow_mod()
        msg_UDP_traceroute = of.ofp_flow_mod()
        msg_ARP.match.dl_type = 0x0806 
        msg_IP.match.dl_type  = 0x0800
        msg_UDP_traceroute.match.dl_type  = 0x0800
        msg_UDP_traceroute.match.nw_proto = ipv4.UDP_PROTOCOL
        msg_ARP.actions.append(of.ofp_action_output(port = of.OFPP_CONTROLLER))
        msg_IP.actions.append(of.ofp_action_output(port = of.OFPP_CONTROLLER))
        msg_UDP_traceroute.actions.append(of.ofp_action_output(port = of.OFPP_CONTROLLER))
        msg_ARP.priority = of.OFP_DEFAULT_PRIORITY - 1
        msg_IP.priority  = of.OFP_DEFAULT_PRIORITY - 1
        msg_UDP_traceroute.priority  = of.OFP_DEFAULT_PRIORITY + 1
        event.connection.send(msg_ARP)
        event.connection.send(msg_IP)
        event.connection.send(msg_UDP_traceroute)

    def _handle_ConnectionDown(self, event):
        connected_switch = self.switches[event.dpid]
        for ip in self.arpmap.keys():
            if self.arpmap[ip][1] == event.dpid:
                del self.arpmap[ip]
        del self.switches[event.dpid]
        for ip in self.arpmap.keys():
            (mac,dpid,port) = self.arpmap[ip]
            if dpid == event.dpid:
                del self.arpmap[ip]
        #let the discovery module deal with the port removals...

    def flood_on_all_switch_edges(self, packet, this_dpid, this_port):
        log.info('flood_on_all_switch_edges')
        for src_dpid in self.switches.keys():
            no_flood = [] #list of non-flood ports
            if src_dpid in self.phys_adjs.keys():
                for nei_dpid in self.phys_adjs[src_dpid]:
                    no_flood.append(self.sw_sw_ports[(src_dpid,nei_dpid)])
            if src_dpid == this_dpid:
                no_flood.append(this_port)
            self.switches[src_dpid].flood_subnet(packet, no_flood)
    
    def send_arp_reply(self, packet, dst_dpid, dst_port, req_mac):
        log.info('send_arp_reply')
        arp_reply = arp()
        arp_reply.hwsrc = req_mac
        arp_reply.hwdst = packet.src
        arp_reply.opcode = arp.REPLY
        arp_reply.protosrc = IPAddr(packet.next.protodst)
        arp_reply.protodst = IPAddr(packet.next.protosrc)
        ether = ethernet()
        ether.type = ethernet.ARP_TYPE
        ether.dst = packet.src
        ether.src = req_mac
        ether.payload = arp_reply
        self.switches[dst_dpid].send_packet_data_created(dst_port, ether)
    
    def update_learned_info(self,packet,dpid,port): #to support host mobility
        log.info('update_learned_info')
        src_ip = None
        src_mac = None
        if packet.type == packet.ARP_TYPE:
            src_mac = EthAddr(packet.src)
            src_ip  = IPAddr(packet.next.protosrc)
        elif packet.type == packet.IP_TYPE:
            src_mac = EthAddr(packet.src)
            src_ip  = IPAddr(packet.next.srcip)
        else:
            log.info("Unknown Packet type: %s" % packet.type)
            pass
        if (src_ip != None) and (src_mac != None):
            self.arpmap[src_ip] = (src_mac, dpid, port)
    
    def _handle_PacketIn(self, event):
        log.info('PacketIn') 
        packet = event.parsed
        dpid = event.dpid
        inport = event.port
        buffer_id = event.ofp.buffer_id
     
        if packet.type == packet.LLDP_TYPE:
            log.info('LLDP_TYPE')

        elif packet.type == packet.ARP_TYPE:
            if packet.next.opcode == arp.REQUEST:
                log.info('ARP REQUEST')
                self.update_learned_info(packet,dpid,inport)
                #print "Received ARP request from %s to %s, updating ARP info..." % (str(packet.next.protosrc), str(packet.next.protodst))
                #print "Current ARP MAP:"
                #print self.arpmap
                if IPAddr(packet.next.protodst) in self.arpmap.keys():
                    #print "I know where to send the crafted ARP reply!"
                    (req_mac, req_dpid, req_port) = self.arpmap[IPAddr(packet.next.protodst)]
                    (dst_mac, dst_dpid, dst_port) = self.arpmap[IPAddr(packet.next.protosrc)]
                    self.send_arp_reply(packet, dst_dpid, dst_port, req_mac)
                else:
                   #print "Flooding initial ARP request on all switch edges"
                   self.flood_on_all_switch_edges(packet,dpid,inport)
                
            elif packet.next.opcode == arp.REPLY:
                log.info('ARP REPLY')                
                self.update_learned_info(packet,dpid,inport)
                #print "Received ARP reply from %s to %s, updating ARP info..." % (str(packet.next.protosrc), str(packet.next.protodst))
                #print "Current ARP MAP:"
                #print self.arpmap
                if IPAddr(packet.next.protodst) in self.arpmap.keys():
                    #print "I know where to send the initial ARP reply!"
                    (dst_mac, dst_dpid, dst_port) = self.arpmap[IPAddr(packet.next.protodst)]
                    self.switches[dst_dpid].send_packet_data_created(dst_port, packet)
                else:
                    #print "Flooding initial ARP reply on all switch edges"
                    self.flood_on_all_switch_edges(packet,dpid,inport)
            else:
                log.info("Unknown ARP type")
                return

        elif packet.type == packet.IP_TYPE:
            log.info('IP_TYPE')
            srcip = IPAddr(packet.next.srcip)
            dstip = IPAddr(packet.next.dstip)
            log.info('PacketIn - srcIP: ' + str(srcip))
            log.info('PacketIn - dstIP: ' + str(dstip))
            log.info('Current DPID: ' + str(dpid))
            if self._computed == True:
                log.info('Routing converged, IP flow can be sent via network') 
                #print "Routing converged, IP flow can be sent via network"
                if (dstip in self.arpmap.keys()) and (srcip in self.arpmap.keys()):
                    (dst_mac, dst_dpid, dst_port) = self.arpmap[dstip]
                    (src_mac, src_dpid, src_port) = self.arpmap[srcip]
                    if packet.next.protocol == packet.next.UDP_PROTOCOL and packet.next.next.dstport in self.traceroute_dst_ports:
                        log.info("traceroute packet")
                        ttl = packet.next.ttl
                        log.info("TTL value: " + str(ttl))
                        if ttl > 1:
                            self.forwardTraceroute(event, packet, srcip, dstip, src_dpid, dst_dpid, ttl)
                        else:
                            self.respondToTraceroute(event, packet, srcip, dstip, src_dpid, dst_dpid, src_port)
                    else:
                        if dpid == src_dpid: 
                            self.install_path(src_dpid, dst_dpid, dst_port, packet)
                        else:
                            log.info('Not source DPID')
                            self.flood_on_all_switch_edges(packet,dpid,inport)   
                else:
                    log.info('ARP mappings not known for both src and dst')
                    self.flood_on_all_switch_edges(packet,dpid,inport)
            else:
                log.info('Routing not converged, IP flow will be handled by proxy controller')
                print "Routing not converged, IP flow will be handled by proxy controller"
                if packet.next.dstip in self.arpmap.keys():
                    (dst_mac, dst_dpid, dst_port) = self.arpmap[packet.next.dstip]
                    self.switches[dst_dpid].send_packet_data_created(dst_port, packet)
                else:  
                    self.flood_on_all_switch_edges(packet,dpid,inport)
        else:
            log.info("Unknown Packet type: %s" % packet.type)
            pass
        return

    def forwardTraceroute(self, event, packet, srcip, dstip, src_dpid, dst_dpid, ttl):
        log.info("forwardTraceroute")
        dpid = event.dpid

        ip_packet = packet.find('ipv4')
        # TTL is greater than 1,
        # decrement TTL
        new_ttl = ttl - 1
        log.info("New TTL vaalue: " + str(new_ttl))
        # generate new IP packet,
        # putting the new TTL value
        new_ip_packet = ipv4(srcip=ip_packet.srcip, dstip=ip_packet.dstip,
                             protocol=ip_packet.protocol, ttl=new_ttl)
        # and copying the old IP payload
        new_ip_packet.payload = ip_packet.next
        ethernet_packet = ethernet(type=packet.IP_TYPE,
                        src=packet.src, dst=packet.dst)
        ethernet_packet.set_payload(new_ip_packet)

        # forward the packet to the next hop
        path = self.find_path(src_dpid, dst_dpid, srcip, dstip)
        #log.info("dpid: %i" % dpid)

        if dpid in path:
            dpid_index = path.index(dpid)
            hops = path[:dpid_index]
            if not hops:
                out_port = self.arpmap[dstip][2]
            elif len(hops) > 1:
                next_hop = hops[-1]
                out_port = self.sw_sw_ports[(dpid,next_hop)]
            else:
                next_hop = hops[0]
                out_port = self.sw_sw_ports[(dpid,next_hop)]
        else:
            next_hop = path[-1]
            out_port = self.sw_sw_ports[(dpid,next_hop)]

        msg = of.ofp_packet_out()
        msg.data = ethernet_packet.pack()
        msg.actions.append(of.ofp_action_output(port = out_port))
        msg.in_port = event.port
        event.connection.send(msg)

    def respondToTraceroute(self, event, packet, srcip, dstip, src_dpid, dst_dpid, final_port):
        log.info("respondToTraceroute")
        dpid = event.dpid
        # TTL equals 1

            ## if dst IP equals switch IP,
            #if packet.next.dstip ==
                # send an ICMP Destination Port Unreachable
                #icmp_packet = icmp(type=pkt.TYPE_DEST_UNREACH, code=pkt.CODE_U$
            #else:
                # else send an ICMP Time Exceeded

        # generate an ICMP Time Exceeded packet
        old_ip_packet = packet.find('ipv4')
        data = old_ip_packet.pack()
        time_exceeded_packet_payload = data[:old_ip_packet.hl * 4 + 8]
        time_exceeded_packet_header = exceed().hdr(time_exceeded_packet_payload)
        time_exceeded_packet = time_exceeded_packet_header + time_exceeded_packet_payload

        icmp_packet = icmp(type=TYPE_TIME_EXCEED, code=0)
        icmp_packet.payload = time_exceeded_packet

        ip_packet = ipv4(protocol=packet.next.ICMP_PROTOCOL,
                        srcip=IPAddr("10.0.1." + str(int(dpid))),
                        dstip=packet.next.srcip)
        ip_packet.payload = icmp_packet

        ethernet_packet = ethernet(type=packet.IP_TYPE,
                src=packet.dst, dst=packet.src)
        ethernet_packet.payload = ip_packet

        # send the packet to the previous hop
        path = self.find_path(src_dpid, dst_dpid, srcip, dstip)
        node_list = path[:]
        node_list.append(src_dpid)

        dpid_index = node_list.index(dpid)
        hops = node_list[dpid_index:][::-1]

        match = of.ofp_match()
        match.dl_type = 0x0800
        match.nw_src = ip_packet.srcip
        match.nw_dst = ip_packet.dstip
        match.nw_proto = ip_packet.ICMP_PROTOCOL

        # install rules to forward it up to the source
        for i, dpid in enumerate(hops):
            if i == 0: #destination dpid
                if len(hops) == 1:
                    self.switches[dpid].send_packet_data_created(final_port,ethernet_packet.pack())
                    log.info('PacketOut from scource')
                log.info('Install flow rule, switch: '+ str(dpid) + ' port: ' + str(final_port))
                self.switches[dpid].install_enqueue_flow_rule(final_port, match, 0, 25)
            else:
                if i == (len(hops)-1): #also packet out from the source so as not to lose the current packets
                    self.switches[dpid].send_packet_data_created(self.sw_sw_ports[(dpid,hops[i-1])],ethernet_packet.pack())
                    log.info('PacketOut from scource')
                log.info('Install flow rule, switch: '+ str(dpid) + ' port: ' + str(self.sw_sw_ports[(dpid,hops[i-1])]))
                self.switches[dpid].install_enqueue_flow_rule(self.sw_sw_ports[(dpid,hops[i-1])], match, 0, 25)
       
        #msg = of.ofp_packet_out()
        #msg.data = ethernet_packet.pack()
        #msg.actions.append(of.ofp_action_output(port = out_port))
        #msg.in_port = event.port
        #event.connection.send(msg)


    def find_path(self, src_dpid, dst_dpid, srcIP, dstIP):

        # if the flow passes from a new end switch pair
        if (src_dpid, dst_dpid) not in self.switches[src_dpid].flowSwitchPairs:
            # then register it to the end switch pair list 
            self.switches[src_dpid].flowSwitchPairs.append((src_dpid, dst_dpid))
            # set the rerouting stage for this destination
            self.switches[src_dpid].rerouting_stage[dst_dpid] = 0
            # register the flow's IP pair to the default end switch pair path
            self.switches[src_dpid].pathsToFlows[(src_dpid, dst_dpid, 0)] = []
            self.switches[src_dpid].pathsToFlows[(src_dpid, dst_dpid, 0)].append((srcIP, dstIP))
            # assign the default/physical topo to this end switch pair
            self.switches[src_dpid].pathsToTopos[(src_dpid, dst_dpid, 0)] = copy.deepcopy(self.switches[src_dpid].pathsToTopos[(0, 0, 0)])
            # get the path for this end switch pair
            self.switches[src_dpid]._paths[(src_dpid, dst_dpid, 0)] = self.switches[src_dpid]._paths[(0, 0, 0)]
            path = self.switches[src_dpid]._paths[(src_dpid, dst_dpid, 0)][dst_dpid]
            log.info('New flow - New DPID pair - Default path')
        else:
            existingFlow = False
            # if the flow passes from an already used end switch pair
            # find if this is an existing flow
            stages = range(self.switches[src_dpid].rerouting_stage[dst_dpid] + 1)
            for stage in stages:
                # if this IP pair (flow) exist in the established paths
                log.info('stage: ' + str(stage))
                print "aaa: ", self.switches[src_dpid].pathsToFlows[(src_dpid, dst_dpid, stage)]
                print "(srcIP, dstIP): ", (srcIP, dstIP)
                if (srcIP, dstIP) in self.switches[src_dpid].pathsToFlows[(src_dpid, dst_dpid, stage)]:
                    #if (src_dpid, dst_dpid, stage) in 
                    path = self.switches[src_dpid]._paths[(src_dpid, dst_dpid, stage)][dst_dpid]
                    existingFlow = True
                    log.info('Existing flow')
                    break
    
            # if this is a new flow passing through 
            # an already used end switch pair
            if existingFlow == False:
                # register the flow's IP pair to the default end switch pair path
                self.switches[src_dpid].pathsToFlows[(src_dpid, dst_dpid, 0)].append((srcIP, dstIP))
                # get the path for this end switch pair
                path = self.switches[src_dpid]._paths[(src_dpid, dst_dpid, 0)][dst_dpid]
                log.info('New flow - New IP pair - Default path')
    
        log.info('Path: ' + str(path))
        return path
    
    def install_path(self, src_dpid, dst_dpid, final_port, packet):
        log.info('install_path')
        #match = of.ofp_match.from_packet(packet)
        srcIP = packet.next.srcip
        dstIP = packet.next.dstip
        
        # get the queue_id to install the 
        # flow to the corresponding queue 
        # of every switch towards the path
        queue_id = 0
        if (srcIP,dstIP) in self.rate_limited_flows:
            queue_id = 1        

        match = of.ofp_match()
        match.dl_type = 0x0800
        match.nw_src = srcIP
        match.nw_dst = dstIP
        match.nw_proto = packet.next.protocol

        match_reverse = of.ofp_match()
        match_reverse.dl_type = 0x0800
        match_reverse.nw_src = dstIP
        match_reverse.nw_dst = srcIP
        match_reverse.nw_proto = packet.next.protocol      

        #log.info('Source IP: ' + str(srcIP))
        #log.info('Dest IP: ' + str(dstIP))
        #log.info('src_dpid: ' + dpid_to_str(src_dpid))
        #log.info('dst_dpid: ' + dpid_to_str(dst_dpid))
    
        path = self.find_path(src_dpid, dst_dpid, srcIP, dstIP)
        reverse_path = self.find_path(dst_dpid, src_dpid, dstIP, srcIP)
        initial_port = self.arpmap[srcIP][2]
        
        node_list = []
        for u in path:
            node_list.append(u)
        node_list.append(src_dpid)
        for i, dpid in enumerate(node_list):
            if i == 0: #destination dpid
                self.switches[dpid].install_enqueue_flow_rule(final_port, match, queue_id, self.flow_rules_idle_timeout)
                log.info('Install flow rule, switch: '+ str(dpid) + ' port: ' + str(final_port))
                # install reverse path
                self.switches[dpid].install_enqueue_flow_rule(self.sw_sw_ports[(dpid,node_list[i+1])], match_reverse, queue_id, self.flow_rules_idle_timeout+2)
                log.info('Install flow rule, switch: '+ str(dpid) + ' port: ' + str(self.sw_sw_ports[(dpid,node_list[i+1])]))
            elif i == (len(node_list)-1):
                self.switches[dpid].install_enqueue_flow_rule(self.sw_sw_ports[(dpid,node_list[i-1])], match, queue_id, self.flow_rules_idle_timeout)
                log.info('Install flow rule, switch: '+ str(dpid) + ' port: ' + str(self.sw_sw_ports[(dpid,node_list[i-1])]))
                # install reverse path
                self.switches[dpid].install_enqueue_flow_rule(initial_port, match_reverse, queue_id, self.flow_rules_idle_timeout+2)
                log.info('Install flow rule, switch: '+ str(dpid) + ' port: ' + str(initial_port))
                # also packet out from the source so as not to lose the current packets
                self.switches[dpid].send_packet_data_created(self.sw_sw_ports[(dpid,node_list[i-1])],packet)
                log.info('PacketOut from scource')
            else:                    
                self.switches[dpid].install_enqueue_flow_rule(self.sw_sw_ports[(dpid,node_list[i-1])], match, queue_id, self.flow_rules_idle_timeout)
                log.info('Install flow rule, switch: '+ str(dpid) + ' port: ' + str(self.sw_sw_ports[(dpid,node_list[i-1])]))
                # install reverse path                   
                self.switches[dpid].install_enqueue_flow_rule(self.sw_sw_ports[(dpid,node_list[i+1])], match_reverse, queue_id, self.flow_rules_idle_timeout+2)
                log.info('Install flow rule, switch: '+ str(dpid) + ' port: ' + str(self.sw_sw_ports[(dpid,node_list[i+1])]))
             
            #if i == 0: #destination dpid
            #    self.switches[dpid].install_output_flow_rule(final_port, match, self.flow_rules_idle_timeout)
            #    log.info('Install flow rule, switch: '+ str(dpid) + ' port: ' + str(final_port))
            #    # install reverse path
            #    self.switches[node_list[len(node_list)-1]].install_output_flow_rule(initial_port, match_reverse, self.flow_rules_idle_timeout+2)
            #    log.info('Install flow rule, switch: '+ str(node_list[len(node_list)-1]) + ' port: ' + str(initial_port))
            #else:
            #    self.switches[dpid].install_output_flow_rule(self.sw_sw_ports[(dpid,node_list[i-1])], match, self.flow_rules_idle_timeout)
            #    log.info('Install flow rule, switch: '+ str(dpid) + ' port: ' + str(self.sw_sw_ports[(dpid,node_list[i-1])]))
            #    # install reverse path                   
            #    self.switches[node_list[len(node_list)-1-i]].install_output_flow_rule(self.sw_sw_ports[(node_list[len(node_list)-1-i],node_list[len(node_list)-i])], match_reverse, self.flow_rules_idle_timeout+2)
            #    log.info('Install flow rule, switch: '+ str(node_list[len(node_list)-1-i]) + ' port: ' + str(self.sw_sw_ports[(node_list[len(node_list)-1-i],node_list[len(node_list)-i])]))
            #    if i == (len(node_list)-1): #also packet out from the source so as not to lose the current packets
            #        self.switches[dpid].send_packet_data_created(self.sw_sw_ports[(dpid,node_list[i-1])],packet)
            #        log.info('PacketOut from scource')


    def checkPaths(self):
        if not self._computed:
            self.dijkstra = Dijkstra(self.switches)
            for source in self.switches.keys():
                for destination in self.switches.keys():
                    if source != destination:
                        self._computed = self.dijkstra.dijkstra(source, destination)
                        if self._computed == False:
                            break
            return self._computed

    def checkPath(self, source, destination):
        if not self._rerouted:
            self.dijkstra = Dijkstra(self.switches)
            self._rerouted = self.dijkstra.dijkstra(source, destination)
        return self._rerouted

    #Careful, need reformulation if assuming multiple links between two dpids
    def _handle_openflow_discovery_LinkEvent(self, event):
        log.info('LinkEvent')
        #self._computed = False
        link = event.link
        dpid1 = link.dpid1
        port1 = link.port1
        dpid2 = link.dpid2
        port2 = link.port2
        if dpid1 not in self.phys_adjs.keys():
            self.phys_adjs[dpid1] = []
        #if dpid2 not in self.adjs.keys():
        #    self.adjs[dpid2] = []
        
        if event.added: 
            if (dpid1,dpid2) not in self.sw_sw_ports:
                self.sw_sw_ports[(dpid1,dpid2)] = port1
                #self.sw_sw_ports[(dpid2,dpid1)] = port2
                self.phys_adjs[dpid1].append(dpid2)
                #self.adjs[dpid2].append(dpid1)
                for sw in self.switches.keys():
                    self.switches[sw].pathsToTopos[(0, 0, 0)] = self.phys_adjs

        	#else:
        	#    del self.sw_sw_ports[(dpid1,dpid2)]
        	#    #del self.sw_sw_ports[(dpid2,dpid1)]
        	#    self.phys_adjs[dpid1].remove(dpid2)
        	#    if len(self.phys_adjs[dpid1]) == 0:
        	#        del self.phys_adjs[dpid1]
        	#    #self.adjs[dpid2].remove(dpid1)
        	#    
        	#    # remove this link from each virtual topo in every switch
        	#    for sw in self.switches.keys():
        	#        for virt_adjs in self.switches[sw].pathsToTopos.values():
        	#            virt_adjs[dpid1].remove(dpid2)                   
	
                log.info("Current switch-to-switch ports:")
        	log.info(self.sw_sw_ports)
        	log.info("Current adjacencies:")
        	log.info(self.phys_adjs)
        	self._computed=False
        	self.checkPaths()
        	if self._computed == False:
            	    log.info("Warning: Disjoint topology, Shortest Path Routing converging")
        	else:
            	    log.info("Topology connected, Dijkstra paths (re)computed successfully, Shortest Path converged")
            	    log.info("--------------------------")
            	    for dpid in self.switches.keys():
                        self.switches[dpid].printPaths()
            	    log.info("--------------------------")
	

    def _handle_link_monitoring_LinkCongestion(self, event):
        log.info('LinkCongestion event received')
        aggr_rerout_info = {}
        #aggr_rerout_flows = {}
        #aggr_bw_per_flow = {}
        #aggr_cong_link = {}
        #con = event.con
        #dpid = con.dpid
        msg_received = event.msg
        traffic_matrix = msg_received['TRAFFIC_MATRIX']
        source_ranking = msg_received['RANKING']
        # congested links
        # list of (dpid1, dpid2)
        congested_links  = msg_received['REROUTING'].keys()
        for congested_link in congested_links:
            for ind, dstIP in enumerate(msg_received['REROUTING'][congested_link]['dstIP']):
                dstIP = self.reformIP(dstIP)
                dst_dpid = self.arpmap[dstIP][1]
                srcIP_list = [self.reformIP(src) for src in msg_received['REROUTING'][congested_link]['srcIPlist'][ind]]
                log.info('ScrIPlist: ' + str(srcIP_list))
                bw = msg_received['REROUTING'][congested_link]['bw'][ind]
                bw_per_flow = bw / float(len(srcIP_list))                 
                
                # aggregate flows based on the
                # (src_dpid, dst_dpid, stage) tuple
                for srcIP in srcIP_list:
                    # Find the switch the traffic 
                    # originates in this domain
                    src_dpid = self.arpmap[srcIP][1]
                    stage = self.switches[src_dpid].rerouting_stage[dst_dpid]
                    # If the source switch has default traffic to this
                    # destination, i.e. hasn't rerouted its traffic
                    if stage == 0:
                        flowStage = 0
                    else:
                        # If the source switch has already rerouted part of its traffic,
                        # find the (src, dst, stage) tuple this flow belongs to
                        stages = range(stage + 1)
                        for stage in stages:
                            # if this IP pair (flow) exist in the established paths
                            if (srcIP, dstIP) in self.switches[src_dpid].pathsToFlows[(src_dpid, dst_dpid, stage)]:
                                flowStage = stage
                                break
                                                         
                    # if the source has already a ranking and 
                    # exceeds a predefined threshold, then rate-limit 
                    # this flow and don't reroute it
                    if srcIP in source_ranking and source_ranking[srcIP] > self.rate_limit_threshold:
                        log.info('Source: '+str(srcIP)+' - Rate-Limited !!')
                        self.rate_limited_flows.append((srcIP,dstIP))
                        # Place the flow to the second queue
                        # with half of the nominal bandwidth
                        # by deleting the existing flow rule
   	                # in the originating switch
   	                match = of.ofp_match()
   	                match.dl_type = 0x0800
   	                match.nw_src = srcIP
   	                match.nw_dst = dstIP
   	                rule = of.ofp_flow_mod()
   	                rule.match = match
   	                rule.command = of.OFPFC_DELETE
   	                core.openflow.sendToDPID(src_dpid, rule)
                    else:
                        #if (src_dpid, dst_dpid, flowStage) not in aggr_rerout_flows.keys():
                        if (src_dpid, dst_dpid, flowStage) not in aggr_rerout_info.keys():
                            #aggr_rerout_flows[(src_dpid, dst_dpid, flowStage)] = []
                            #aggr_bw_per_flow[(src_dpid, dst_dpid, flowStage)] = []
                            #aggr_cong_link[(src_dpid, dst_dpid, flowStage)] = []
                            aggr_rerout_info[(src_dpid,dst_dpid,flowStage)] = []
                        #aggr_bw_per_flow[(src_dpid, dst_dpid, flowStage)].append(bw_per_flow)
                        #aggr_rerout_flows[(src_dpid, dst_dpid, flowStage)].append((srcIP, dstIP))
                        #aggr_cong_link[(src_dpid, dst_dpid, flowStage)].append(congested_link)

                        # Aggregate flows 
                        # Each flow has (srcIP,dstIP), bw_per_flow, congested_link
                        # as a tuple
                        aggr_rerout_info[(src_dpid,dst_dpid,flowStage)].append(((srcIP,dstIP),bw_per_flow,congested_link))

        log.info('aggr_rerout_info: '+str(aggr_rerout_info))
        msg_response_monitoring = {}
        msg_response_server = {}
        # for each flow having the same
        # (src_dpid, dst_dpid, stage) tuple
        #for flow_key in aggr_rerout_flows.keys():
        for flow_key in aggr_rerout_info.keys():
            src_dpid = flow_key[0]
            dst_dpid = flow_key[1]
            log.info('flow_key: '+str(flow_key))
            log.info(str(aggr_rerout_info[flow_key]))
            limited_bw_links = []
            retry = 0
            rerouting_available = True
            rerouted_flows = 0
            #aggr_rerout_info_copy = {}
            #aggr_rerout_info_copy[flow_key] = []
            #aggr_rerout_info_copy[flow_key] = copy.deepcopy(aggr_rerout_info[flow_key])
            #log.info('WHILE aggr_rerout_flows['+str(flow_key)+']: '+str(aggr_rerout_flows[flow_key]))
            while aggr_rerout_info[flow_key] and rerouting_available and retry < 3:
                sw_rerouting_stage = self.switches[src_dpid].rerouting_stage[dst_dpid]
                retry = retry + 1
                rerouting_available = False
                # get a copy of the topo for this flow
                virt_adjs = copy.deepcopy(self.switches[src_dpid].pathsToTopos[flow_key])            
                # remove all the target links from the topo
                for link in congested_links:
                    virt_adjs[link[0]].remove(link[1])
                log.info('virt_adjs_modified - TARGET LINKS: ' + str(virt_adjs))
                # remove all links with limited bandwidth
                if limited_bw_links:
                    for link in limited_bw_links:
                        virt_adjs[link[0]].remove(link[1])
                    limited_bw_links = []
                    log.info('virt_adjs_modified - LIMITED BW: ' + str(virt_adjs))
                # update the rerouting stage
                new_sw_rerout_stage = sw_rerouting_stage + 1
                self.switches[src_dpid].rerouting_stage[dst_dpid] = new_sw_rerout_stage
                # Store the modified virtual topology 
                self.switches[src_dpid].pathsToTopos[(src_dpid, dst_dpid, new_sw_rerout_stage)] = copy.deepcopy(virt_adjs)
                # check if the new rerouting is possible
                self._rerouted = False
                self.checkPath(src_dpid, dst_dpid)
                if self._rerouted:
                    rerouting_available = True
                    # Check if the bandwidth in the 
                    # new path allows the rerouting
                    path = self.switches[src_dpid]._paths[(src_dpid, dst_dpid, new_sw_rerout_stage)][dst_dpid]
                    log.info('Src: '+str(src_dpid)+' Dst:'+str(dst_dpid)+' NEW path:' + str(path))
                    node_list = [u for u in path]
                    node_list.append(src_dpid)
                    path_available_bw = [traffic_matrix[(node_list[i],node_list[i-1])] for i in range(1,len(node_list))]
                    ## sort the lists
                    #aggr_bw_per_flow[flow_key], aggr_rerout_flows[flow_key], aggr_cong_link[flow_key] = (list(t) for t in zip(*sorted(zip(aggr_bw_per_flow[flow_key],aggr_rerout_flows[flow_key],aggr_cong_link[flow_key]),reverse=True)))
                    # sort the list by highest bandwidth
                    aggr_rerout_info[flow_key] = sorted(aggr_rerout_info[flow_key],key=lambda x: x[1],reverse=True)
                    fit = True
                    index = 0
                    rerouted = False
                    #aggr_bw_per_flow_copy = {}
                    #aggr_bw_per_flow_copy[flow_key] = []
                    aggr_rerout_info_copy = {}
                    aggr_rerout_info_copy[flow_key] = []
                    log.info('aggr_rerout_info[str(flow_key)]: '+str(aggr_rerout_info[flow_key]))
                    #aggr_bw_per_flow_copy[flow_key] = copy.deepcopy(aggr_bw_per_flow[flow_key])
                    aggr_rerout_info_copy[flow_key] = copy.deepcopy(aggr_rerout_info[flow_key])
                    #for ind, bw in enumerate(aggr_bw_per_flow_copy[flow_key]):
                    for (flow,bw,congested_link) in aggr_rerout_info_copy[flow_key]:
                        if bw < min(path_available_bw):
                            # reroute this flow
                            #flow = aggr_rerout_flows[flow_key][ind]
                            ########################################
                            old_path = self.find_path(src_dpid,dst_dpid,flow[0],flow[1])
                            old_node_list = [u for u in old_path]
                            old_node_list.append(src_dpid)
                            old_hops = old_node_list[::-1] 
                            old_links = zip(old_hops[::1],old_hops[1::1])
                            #######################################
                            self.reroute_flow(flow_key, flow)
                            # 
                            #congested_link = aggr_cong_link[flow_key][ind]
                            if congested_link not in msg_response_monitoring:
                                log.info("1==================")
                                msg_response_monitoring[congested_link] = {}
                                msg_response_monitoring[congested_link]['REROUTING_LINKS'] = []
                                msg_response_monitoring[congested_link]['SW'] = {}

                            rerouted_hops = node_list[::-1]
                            rerouted_links = zip(rerouted_hops[::1],rerouted_hops[1::1])
                            #################
                            disjoint_rerouted_links = list(set(rerouted_links)-set(old_links))
                            log.info('Disjoint rerouted links: '+str(disjoint_rerouted_links))
                            #################
                            #for link in rerouted_links:
                            for link in disjoint_rerouted_links:
                                if link not in msg_response_monitoring[congested_link]['REROUTING_LINKS']:
                                    msg_response_monitoring[congested_link]['REROUTING_LINKS'].append(link)
                                    log.info("2==================")
                            # get the entry link of the rerouted path
                            # to monitor for returning sources of 
                            # the rerouted destinations/flows
                            # reverse_entry_hops : [dpid2, dpid1]
                            reverse_entry_hops = node_list[-2:]
                            entry_link = reverse_entry_hops[1]
                            entry_link_port = self.sw_sw_ports[(entry_link,reverse_entry_hops[0])]
                            #if (entry_link,entry_link_port) not in msg_response_monitoring[congested_link]:
                            if (entry_link,entry_link_port) not in msg_response_monitoring[congested_link]['SW']:
                                msg_response_monitoring[congested_link]['SW'][(entry_link,entry_link_port)] = {}
                                #msg_response_monitoring[congested_link][(entry_link,entry_link_port)] = {}
                                msg_response_monitoring[congested_link]['SW'][(entry_link,entry_link_port)]['dstIP'] = []
                                #msg_response_monitoring[congested_link][(entry_link,entry_link_port)]['dstIP'] = []
                                msg_response_monitoring[congested_link]['SW'][(entry_link,entry_link_port)]['srcIPlist'] = []
                                #msg_response_monitoring[congested_link][(entry_link,entry_link_port)]['srcIPlist'] = []
                                log.info("3==================")

                            # get the source and destination IP 
                            # of the rerouted flow 
                            scr_ip, dst_ip = flow

                            #if dst_ip not in msg_response_monitoring[congested_link][(entry_link,entry_link_port)]['dstIP']:
                            if dst_ip not in msg_response_monitoring[congested_link]['SW'][(entry_link,entry_link_port)]['dstIP']:
                                msg_response_monitoring[congested_link]['SW'][(entry_link,entry_link_port)]['dstIP'].append(dst_ip)
                                #msg_response_monitoring[congested_link][(entry_link,entry_link_port)]['dstIP'].append(dst_ip)
                                j = len(msg_response_monitoring[congested_link]['SW'][(entry_link,entry_link_port)]['dstIP']) - 1
                                #j = len(msg_response_monitoring[congested_link][(entry_link,entry_link_port)]['dstIP']) - 1
                                msg_response_monitoring[congested_link]['SW'][(entry_link,entry_link_port)]['srcIPlist'].append([])
                                #msg_response_monitoring[congested_link][(entry_link,entry_link_port)]['srcIPlist'].append([])
                            else:
                                j = msg_response_monitoring[congested_link]['SW'][(entry_link,entry_link_port)]['dstIP'].index(dst_ip)
                                #j = msg_response_monitoring[congested_link][(entry_link,entry_link_port)]['dstIP'].index(dst_ip)
                            msg_response_monitoring[congested_link]['SW'][(entry_link,entry_link_port)]['srcIPlist'][j].append(scr_ip)
                            #msg_response_monitoring[congested_link][(entry_link,entry_link_port)]['srcIPlist'][j].append(scr_ip)

                            rerouted = True
                            rerouted_flows = rerouted_flows + 1    
                            # delete the appropriate entries
                            #aggr_bw_per_flow[flow_key].remove(bw)
                            #del aggr_rerout_flows[flow_key][ind]
                            #del aggr_cong_link[flow_key][ind]
                            aggr_rerout_info[flow_key].remove((flow,bw,congested_link))
                            # update available BW
                            path_available_bw = [i-bw for i in path_available_bw]
                            # update traffic matrix
                            for i in range(1,len(node_list)):
                                traffic_matrix[(node_list[i],node_list[i-1])] = traffic_matrix[(node_list[i],node_list[i-1])] - bw
                        else:
                            fit = False
                            # store the links that do not fit
                            # in a list of lists
                            limited_bw[index] = []
                            for i in range(1,len(node_list)):
                                if bw > traffic_matrix[(node_list[i],node_list[i-1])]:
                                    limited_bw[index].append((node_list[i],node_list[i-1]))
                            index = index + 1
                    #log.info('aggr_bw_per_flow['+str(flow_key)+']: '+str(aggr_bw_per_flow[flow_key]))
                    #aggr_bw_per_flow_copy[flow_key] = aggr_bw_per_flow[flow_key][:]
                    if not fit:
                        # intersection of links to cut
                        limited_bw_links = list(set.intersection(*map(set, limited_bw)))
                    if not rerouted:
                        # remove the new virtual topology
	                # as the new rerouting is not possible
   	                del self.switches[src_dpid].pathsToTopos[(src_dpid, dst_dpid, new_sw_rerout_stage)]
   	                # the rerouting stage is not updated,
   	                # as the rerouting is not possible
   	                self.switches[src_dpid].rerouting_stage[dst] = sw_rerouting_stage
            if rerouted_flows == 0:
                # store the unrerouted flows
                # then
                #msg_response_server = 
                log.info('Contact the ISP')         
                log.info('for these unrerouted flows')
                  
   	    else:
                log.info('Some/all reported flows were rerouted')
                #msg_response_server[flow_key] = {}
                #msg_response_server[flow_key]['FLOWS'] = aggr_rerout_flows[flow_key]
   	        #print "Warning: Disjoint topology"
   	        #log.info('Couldn\'t find alternate path for this flow')
   	        #log.info('------------ THE ISP HAS TO BE CONTACTED!!!! --------------')
                
        # If rerouting is successful,
        # raise ReroutingMonitoring Event
        # Info passed to Monitoring component
        # to check for returing sources  
        if msg_response_monitoring:
            # WRITE TO FILE
            #self.defence_complete_file.write('CONGESTION_MITIGATED: '+str(time.time())+'\n')
            #self.defence_complete_file.flush()
            log.info('msg_response_monitoring: '+str(msg_response_monitoring))
            log.info('ReroutingMonitoring event raised')
            self.raiseEvent(ReroutingMonitoring,msg_response_monitoring)
        #if not msg_response_server:
        #    pass
            #msg_response_server
                     
   	    print "Current switch-to-switch ports:"
   	    print self.sw_sw_ports
   	    print "Current adjacencies:"
   	    print self.phys_adjs
   	    print "--------------------------"
   	    for dpid in self.switches.keys():
   	        self.switches[dpid].printPaths()
   	    print "--------------------------"

    def reroute_flow(self, flow_key, flow):
        src_dpid, dst_dpid, stage = flow_key
        # remove the flow from its path list
        log.info("self.switches[src_dpid].pathsToFlows[flow_key]: "+str(self.switches[src_dpid].pathsToFlows[flow_key]))
        log.info("flow: "+str(flow))
        self.switches[src_dpid].pathsToFlows[flow_key].remove(flow)
        log.info("self.switches[src_dpid].pathsToFlows[flow_key] MODIFIED: "+str(self.switches[src_dpid].pathsToFlows[flow_key]))
        # add the flow to the new path list
        log.info('REROUTED @@@@@: ' + str(flow_key))
        log.info('REROUTED @@@@@: ' + str(flow))
        new_sw_rerouting_stage = self.switches[src_dpid].rerouting_stage[dst_dpid]
        if (src_dpid, dst_dpid, new_sw_rerouting_stage) not in self.switches[src_dpid].pathsToFlows.keys():
            log.info('REROUTED @@@@@@@@@@@@@@@@@@@@@')
            self.switches[src_dpid].pathsToFlows[(src_dpid, dst_dpid, new_sw_rerouting_stage)] = []
   	self.switches[src_dpid].pathsToFlows[(src_dpid, dst_dpid, new_sw_rerouting_stage)].append(flow)
   	log.info("self.switches[src_dpid].pathsToFlows[(src_dpid, dst_dpid, new_sw_rerouting_stage)].append(flow) "+str(self.switches[src_dpid].pathsToFlows[(src_dpid, dst_dpid, new_sw_rerouting_stage)]))
   	# Reroute the flow by deleting the flow rule
   	# in the originating switch
   	match = of.ofp_match()
   	match.dl_type = 0x0800
   	match.nw_src = flow[0]
   	match.nw_dst = flow[1]
   	rule = of.ofp_flow_mod()
   	rule.match = match
   	rule.command = of.OFPFC_DELETE
   	core.openflow.sendToDPID(src_dpid, rule)

    def reformIP(self, value):
        ip_str = str(value)
        if '/' in ip_str:
            ip_str = ip_str.split('/')[0]
        ip = IPAddr(ip_str)
        return ip
    
    def open_results_files(self):
        # /home/mininet/pox is the CWD
        results_full_path = os.path.join(os.getcwd(),"ext","crossfire_pox","results")
        reaction_times_full_path = os.path.join(results_full_path,"reaction_times")
        defender_reaction_times_full_path = os.path.join(reaction_times_full_path,"defender")
        defence_complete_file_path = os.path.join(defender_reaction_times_full_path,"defence_complete.txt")
        self.defence_complete_file = open(defence_complete_file_path,"w")

    def __str__(self):
        return "Dijkstra Routing"

class SwitchWithPaths (EventMixin):
    def __init__(self):
        self.connection = None
        self.dpid = None
        self.ports = None
        self._listeners = None
        self._paths = {}
        self.rerouting_stage = {}
        self.flowSwitchPairs = []
        self.pathsToFlows = {}
        self.pathsToTopos = {}

    def __repr__(self):
        return dpidToStr(self.dpid)

    def appendPath(self, key, dst, hop):
        #log.info('appendPath')
        if key not in self._paths:
            self._paths[key] = {}
        if dst not in self._paths[key]:
            self._paths[key][dst] = []
        self._paths[key][dst].append(hop)

    def clearPaths(self, key, dst):
        if key not in self._paths:
            self._paths[key] = {}
        self._paths[key][dst] = []

    def printPaths(self):
        for flow in self._paths.keys():
            #log.info('Flow: %s' % (str(flow))) 
            for dst in self._paths[flow].keys():
                log.info("Shortest path from %i to %i:" % (self.dpid,dst))
                #print "%i" % (self.dpid),
                print "%s" % (dpid_to_str(self.dpid)),
                log.info("%s" % (dpid_to_str(self.dpid)))
                for u in self._paths[flow][dst][::-1]:
                    #print "%i" % (u),
                    print "%s" % (dpid_to_str(u)),
                    log.info("%s" % (dpid_to_str(u)))
                print ""

    def connect(self, connection):
        if self.dpid is None:
            self.dpid = connection.dpid
        assert self.dpid == connection.dpid
        if self.ports is None:
            self.ports = connection.features.ports
        #log.info("Connect %s" % (connection))
        self.connection = connection
        self._listeners = self.listenTo(connection)

    def disconnect(self):
        if self.connection is not None:
            #log.info("Disconnect %s" % (self.connection))
            self.connection.removeListeners(self._listeners)
            self.connection = None
            self._listeners = None

    def flood_subnet(self, packet, no_flood):
        for port in self.ports:
            if (not (port.port_no in no_flood)) and (port.port_no <= MAX_PHYS_PORTS):
                self.send_packet_data_created(port.port_no, packet)
    
    def send_packet_data_created(self, outport, data=None):
        msg = of.ofp_packet_out(in_port=of.OFPP_NONE)
        msg.data = data
        msg.actions.append(of.ofp_action_output(port=outport))
        self.connection.send(msg)

    #def install_output_flow_rule(self, outport, match, idle_timeout=0, hard_timeout=0):
    #    log.info('install_output_flow_rule')
    #    #self.connection.send(of.ofp_barrier_request())
    #    msg=of.ofp_flow_mod()
    #    msg.command = of.OFPFC_MODIFY_STRICT
    #    msg.match = match
    #    msg.flags = 2 #check overlap
    #    msg.idle_timeout = idle_timeout
    #    msg.hard_timeout = hard_timeout
    #    msg.actions.append(of.ofp_action_output(port=outport))
    #    self.connection.send(msg)
    
    def install_enqueue_flow_rule(self, outport, match, q_id=0, idle_timeout=0, hard_timeout=0):
        log.info('install_enqueue_flow_rule - QUEUE: '+str(q_id))
        #self.connection.send(of.ofp_barrier_request())
        msg=of.ofp_flow_mod()
        msg.command = of.OFPFC_MODIFY_STRICT
        msg.match = match
        msg.flags = 2 #check overlap
        msg.idle_timeout = idle_timeout
        msg.hard_timeout = hard_timeout
        msg.actions.append(of.ofp_action_enqueue(port=outport,queue_id=q_id))
        self.connection.send(msg) 
    
    def _handle_ConnectionDown(self, event):
        self.disconnect()
    

class Dijkstra(object):

    def __init__(self, switches):
        self.switches = switches

    def init_dijk_round(self):
        dist = {}
        prev = {}
        for v in self.switches.keys():
            dist[v] = sys.maxint #simulate "infinity"
            prev[v] = None
        return (dist, prev)

    def closest(self, Q, dist):
        min = sys.maxint
        closest_node = None
        for u in Q:
            if dist[u] < min:
                min = dist[u]
                closest_node = u
        return closest_node
        

    def dijkstra(self, source, destination): #http://en.wikipedia.org/wiki/Dijkstra's_algorithm
        log.info('dijkstra') 
        #for source in self.switches.keys():
        log.info("SOURCE:"+str(source))
        for key, adjs in self.switches[source].pathsToTopos.iteritems():
            #log.info("KEY:"+str(key))
            #log.info("ADJS:"+str(adjs))
            (dist, prev) = self.init_dijk_round()
            dist[source] = 0
            Q = self.switches.keys()[:]
            while (len(Q)>0):
                u = self.closest(Q, dist)
                if (u is None):
                #log.info("Dijkstra: Disjoint Topology!!!")
                    return False
                Q.remove(u)

                if u not in adjs.keys():
                    return False
                for v in adjs[u]:
                    new_dist = dist[u] + 1
                    if new_dist < dist[v]:
                        dist[v] = new_dist
                        prev[v] = u

            self.switches[source].clearPaths(key, destination)
            if destination != source:
                u = destination
                while (prev[u]):
                    self.switches[source].appendPath(key, destination, u)
                    u = prev[u]
        return True

def launch():
    log.info("Loading Dijkstra Routing module")
    if not core.hasComponent("DijkstraRouting"):
        core.register("DijkstraRouting", DijkstraRouting())
