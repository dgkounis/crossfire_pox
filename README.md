This software emulates the Crossfire attack using Mininet and POX and implements 
a defence approach. It is associated with the following thesis and publication:
1) Similar code used on ftp://ftp.tik.ee.ethz.ch/pub/students/2013-HS/MA-2013-18.pdf
2) This defence mechanism is an early version of the algorithm on:
   D. Gkounis, V. Kotronis, C. Liaskos, X. Dimitropoulos, "On the Interplay of 
   Link-Flooding Attacks and Traffic Engineering", to be published on ACM 
   SIGCOMM CCR April 2016 edition
  

Requirements:
 - POX carp branch
 - Mininet version 2.0.0

Installation:
 - Please download the source code of the repo under pox/ext

High-level explanation of the various scripts:
 - The developed scripts are categorized into the defender, attacker and the 
   auxiliary ones.
   A) The defender scripts include:
      1) The monitoring.py script within the monitoring folder:
         #It polls the connected ports of the switches of the topology every 10 
         #seconds. Based on the OpenFlow Statistics, an approximation of the Traffic 
         #Matrix is done.
         #It receives information from the routing component about the previously 
         #flooded links and the corresponding 
         #It also monitors 

      2) The routing.py script within the routing folder.
         #Receives notification about a flooded event and also 

   B) The attacker scripts are integrated within the script that launches the 
      sample topology (see figure sampleTopo.png).
      # generate HTTP GET traffic using Scapy: sender_script.py folder: 
      #traffic_generators/ attacker.py user.py

   C) The discovery.py script and icmp.py 

 
For more details about the scripts, please read Chapter 4 of 
ftp://ftp.tik.ee.ethz.ch/pub/students/2013-HS/MA-2013-18.pdf



How to run the Crossfire attack and defence:
 1) You need to launch 3 terminals: 1 for POX, 1 for Mininet, 1 for killing the test
 2) Go to crossfire_pox/mininet/ folder and run: sudo python sampleTopo.py. 
    This script launches the topology shown in sampleTopo.png. If you want to 
    use a different topology, then you need to modify the class ReroutingTopo 
    and the function define_allowed_servers accordingly
 3) Copy the script crossfire_pox/mininet/run_pox.sh to pox/ folder and then
    run POX controller and the Crossfire defence POX application using:
    ./run_pox.sh 
 4) When the test is done, use sudo clear_test.py within crossfire_pox/mininet 
    folder to clear everything (POX and Mininet and related files and configs)

Contact:
 Please contact Dimitrios Gkounis (dgkounis@gmail.com) for any questions
