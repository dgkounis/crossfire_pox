"""
Used in combination with sampleTopo.py in mininet folder
"""
from pox.core import core
from pox.openflow import *
import pox.openflow.libopenflow_01 as of
from pox.openflow.discovery import *
from pox.lib.recoco import Timer
from pox.lib.util import dpid_to_str
from pox.openflow.of_json import *
from pox.lib.revent import *

from itertools import combinations
import random
import copy
import time
import os 

log = core.getLogger()

class LinkCongestion(Event):
    """ Fired on a link when a congestion occurs """
    #def __init__(self, con, msg):    
    def __init__(self, msg):    
        Event.__init__(self)
        #self.con = con
        self.msg = msg

class LinkMonitoring (EventMixin):

    _core_name = 'link_monitoring'
    _neededComponents = set(['openflow_discovery'])
    _eventMixin_events = set([LinkCongestion])
    measur_interv = 2
    # 
    rerouting_bw_threshold_percent = 0.40   

    def __init__(self):
        log.info("LinkLossMonitoring __init__")
        super(EventMixin, self).__init__()
        self.link_up = {} # link is up, (dpid1, port1) -> dpid2
        self.link_stats = {} # current measurement link stats, link stats dict
        self.link_used_bw = {} # 
        # current aggregate destination stats (to be compared with the new ones): 
        # congested link and destIP through that link (dpid, port, dest) -> 
        # dict with packet_count and byte_count as keys
        self.aggr_dest_stats = {} 
        # flow_dict: congested link (dpid1,port1) -> dict with destIPs as keys
        # and list of the corresponding sourceIPs as values
        self.flow_dict = {} 
        # The dest_bw and dest_flows dicts store all the past bandwidth 
        # and number of flows measurements in order to determine their 
        # average values  
        # congested link (dpid, port), destination IP (dest)        
        # (dpid,port,dest) -> list [BW1, BW2, ...]
        # (dpid,port,dest) -> list [#FLOWS1, #FLOWS2, ...]
        self.dest_bw = {}
        self.dest_flows = {}
        # congested link (dpid,port) -> dict with destIP as key 
        # and average BW or average # of flows as values
        # These dicts use the lists of the previous two dicts
        # to calculate their values
        self.bw = {}
        self.flows = {}
        # read the nominal BW of each link of the topo
        # nominal BW accessed by the self.link_nominal_bw dict
	self.read_topo_link_capacities()
        # open the files to write the results
        #self.open_results_files()
        self.read_target_links()
        # dictionary that stores previously congested links'
        # rerouting information in order to monitor for 
        # returning sources
        self.monitor_congested_links_dict = {}
        # dictionary that stores all the information about
        # the cyrrent congested links before the info about what to 
        # report to the Routing component is processed
        self.current_congestions_info_dict = {}
        # dictionary that keeps the ranking of the sources
        # (starting from rank=1)
        # srcIP -> ranking 
        self.source_ranking = {}
        # 
        self.correlation_dict = {}        
        # listen to dependent components
        core.listen_to_dependencies(self, self._neededComponents)
        #while not core.listen_to_dependencies(self, self._neededComponents):
        #    self.listenTo(core)
        #self.listenTo(core.openflow)

        # time for estimating the bandwidth and the number 
        # of the aggrerate flows
        self.seconds = 3.0 
        self.start_timer = True
        self.start_measurements = 30
        self.concurrent_congested_links = {}
        self.concurrent_congested_links_number = 0
        self.concurrent_congested_time_interval = 25.0
        self.concurrent_congested_link_time_interval = 5.0
        self.aggr_stats_measure_interval = 1.0
        self.target_links = {}
        self.estimation_weights = (0.6, 0.25, 0.15)
        self.aggr_time_measure = {}
        self.xidToLink = {}
        self.correlatingXids = []
        self.congestedLinkMeasuredDests = {}
        self.concurrent_congested_links_interval_expired = False
        self.expected_target_links = []


    def _all_dependencies_met (self):
        # when all the depending components are registered 
        self.listenTo(core.openflow)
        #pass

    def _handle_openflow_discovery_LinkEvent(self, event):
        log.info("openflow_discovery_LinkEvent")
        # characteristics of a link
        link = event.link
        dpid1 = link.dpid1
        port1 = link.port1
        dpid2 = link.dpid2
        
        if self.start_timer == True:
            self.start_timer = False
            Timer(self.start_measurements, self.measurements_initialization)
                    
        if event.added:
            if (dpid1, port1) not in self.link_up:
                # if a link is added, then a corresponding 
                # entry is also added in a dictionary
                self.link_up[(dpid1, port1)] = dpid2
                # initialization of link stats dict
                self.link_stats[(dpid1, port1)] = {'rx_packets': 0, 'rx_dropped': 0,
                    'rx_bytes': 0, 'rx_errors': 0, 'tx_packets': 0, 'tx_dropped': 0,
                    'tx_bytes': 0, 'tx_errors': 0}
        #else:
            # if a link is removed, then the corresponding
            # entry is removed from the link dictionary 
            #del self.link_up[(dpid1, port1)]
            # and also from the port stats dictionary
            #del self.link_stats[(dpid1, port1)]
    
    def measurements_initialization(self):
        # This link is monitored every #measur_interv# seconds
        self.final_measur_interv = {}
        for link in self.link_up.iterkeys():
            dpid1, port1 = link
            self.final_measur_interv[link] = (self.measur_interv - 0.1) + 0.1 * random.random()
            Timer(self.final_measur_interv[link], self.send_link_stats_request, args=[dpid1, port1], recurring=True)
        log.info("final_measur_interv:" + str(self.final_measur_interv))

    def send_link_stats_request(self, dpid, port):
        log.info("send_link_stats_request")
        if (dpid, port) in self.link_up:
            # if this link still exists, then request for port stats        
            core.openflow.sendToDPID(dpid, of.ofp_stats_request(body=of.ofp_port_stats_request(port_no = port)))
            log.info("Requested port stats from switch, port: %s, %s" %(dpidToStr(dpid), str(port)))
        else:
            # if this link is removed, then 
            # stop requesting for port stats
            log.info("STOP Requesting port stats from switch, port: %s, %s" %(str(dpid), str(port)))
            return False

    def _handle_PortStatsReceived(self, event):
        # handler to display port statistics received in JSON format
        log.info("_handle_PortStatsReceived")
        # DPID of the queried switch/link
        dpid = event.connection.dpid
        # Port Stats dictionary
        stats_dict = flow_stats_to_list(event.stats)[0]
        #log.info("PortStatsReceived from %s: %s", dpidToStr(dpid), stats_dict)
        # Port of the corresponding queried link
        port = stats_dict['port_no']
        # Current port statistics, i.e. between two measurement intervals
        current_tx_packets = stats_dict['tx_packets'] - self.link_stats[(dpid, port)]['tx_packets']
        current_tx_bytes = stats_dict['tx_bytes'] - self.link_stats[(dpid, port)]['tx_bytes']
        if current_tx_packets:
            current_bandwidth = current_tx_bytes * 8 / self.final_measur_interv[(dpid,port)]
        else:
            current_bandwidth = 0.0     
        self.link_used_bw[(dpid,port)] = current_bandwidth
        #log.info("PortStatsReceived from %s: %s", dpidToStr(dpid), stats_dict)
        log.info("=====================================")
        log.info("link DPID: "+str(dpid)+" - Port: "+str(port))
        log.info("current_tx_bytes: "+str(current_tx_bytes))         
        log.info("current_tx_packets: "+str(current_tx_packets))         
        log.info("current_bandwidth: "+str(current_bandwidth))
        log.info("current_interval: "+str(self.final_measur_interv[(dpid,port)]))
        log.info("=====================================")
        # update link statictics with current stats received
        self.link_stats[(dpid, port)] = stats_dict

        dpid2 = self.link_up[(dpid,port)]
        if current_bandwidth > 0.95 * self.link_nominal_bw[tuple(sorted((dpid,dpid2)))]:
            if (dpid,port) not in self.target_links:
                self.target_links[(dpid,port)] = time.time()
                self.group_concurrent_target_links((dpid,port))
                core.openflow.sendToDPID(dpid, of.ofp_stats_request(body=of.ofp_flow_stats_request(out_port = port)))
                log.info("New Target Link !!!!!!!!!!!!!!")
                log.info('LINK_FLOODED: '+str(time.time()))
                if (int(dpid),int(dpid2)) in self.expected_target_links:
                    self.expected_target_links.remove((int(dpid),int(dpid2)))
                    if not self.expected_target_links:
                        # WRITE TO FILE
                        #self.attack_complete_file.write('LAST_TARGET_LINK_FLOODED: '+str(time.time())+'\n')
                        #self.attack_complete_file.flush()
                        pass
            else:
                #if time.time() > self.target_links[(dpid,port)] + self.concurrent_congested_time_interval:
                    #self.target_links[(dpid,port)] = time.time()
                    #self.group_concurrent_target_links((dpid,port))
                    #core.openflow.sendToDPID(dpid, of.ofp_stats_request(body=of.ofp_flow_stats_request(out_port = port)))
                    #log.info("Same Target Link - Interval expired !!!!!!!!!!!!!!")
                #else:
                    #log.info("Same concurrent Target Link !!!!!!!!!!!!!!")
                log.info("Same concurrent Target Link !!!!!!!!!!!!!!")


    def group_concurrent_target_links(self, link):
        # first initial congestion of the test
        if self.concurrent_congested_links_number == 0:
            self.congested_link_time = time.time()
            self.concurrent_congested_links[self.congested_link_time] = []
            self.concurrent_congested_links[self.congested_link_time].append(link)
            # set timer to execute when interval 
            # that concurrent congestions are 
            # handled in a group is passed.
            # The called function will check if 
            # the aggregate stats are all gathered
            # so as to be able to do the source ranking
            log.info("Timer set: "+str(self.congested_link_time))
            Timer(self.concurrent_congested_link_time_interval, self.check_aggr_stats_finished, args=[self.congested_link_time])
        else:
            # concurrent congestions
            if time.time() - self.congested_link_time < self.concurrent_congested_link_time_interval:
                self.concurrent_congested_links[self.congested_link_time].append(link)
                log.info('Concurrent congestion!!!!')
            else:
                # new initial congestion
                self.concurrent_congested_links_number = 0
                self.congested_link_time = time.time()
                log.info("New initial congestion!!!!")
                self.concurrent_congested_links[self.congested_link_time] = []
                self.concurrent_congested_links[self.congested_link_time].append(link)
                # set timer to execute when interval 
                # that concurrent congestions are 
                # handled in a group is passed.
                # The called function will check if 
                # the aggregate stats are all gathered
                # so as to be able to do the source ranking
                log.info("Timer set: "+str(self.congested_link_time))
                Timer(self.concurrent_congested_link_time_interval, self.check_aggr_stats_finished, args=[self.congested_link_time])
        self.concurrent_congested_links_number = self.concurrent_congested_links_number + 1


    def _handle_FlowStatsReceived(self, event):
        log.info("_handle_FlowStatsReceived")
        con = event.connection
        dpid1 = con.dpid 
        port1 = None

        correlating_flow_stats = False
        last_correlating_flow_stats = False
        # if FlowStats for determining 
        # about the returning sources
        if event.ofp[0].xid in self.correlatingXids:
            correlating_flow_stats = True
            log.info("Correlating flow stats")
            # remove entry after using it
            self.correlatingXids.remove(event.ofp[0].xid)
            # if the last FlowStats for all previous
            # target links are received, then 
            # process the corresponding results 
            # to identify returning sources
            if not self.correlatingXids:
                last_correlating_flow_stats = True
                log.info("Last correlating flow stats")
        # Process the FlowStats
        for f in event.stats:
            # IP traffic and TCP traffic
            # nw_proto = {'TCP':6,'UDP':17,'ICMP':1}
            #if f.match.dl_type == 0x0800 and f.match.nw_proto == 6:
            if f.match.dl_type == 0x0800 and f.match.nw_proto != 1 and f.match.nw_proto != 17:
                log.info("flow_match_dict: "+str(match_to_dict(f.match)))
                sourceIP = self.reformIP(match_to_dict(f.match)['nw_src'])
                log.info("source: "+str(sourceIP))
                destIP = self.reformIP(match_to_dict(f.match)['nw_dst'])
                log.info("destination: "+str(destIP))
                port1 = action_to_dict(f.actions[0])['port']
                # if FlowStats for determining 
                # about the returning sources 
                if correlating_flow_stats:
                    if (dpid1, port1) not in self.correlation_dict:
                        self.correlation_dict[(dpid1,port1)] = {}
                    if destIP not in self.correlation_dict[(dpid1,port1)]:
                        self.correlation_dict[(dpid1,port1)][destIP] = []
                    self.correlation_dict[(dpid1,port1)][destIP].append(sourceIP)
                else:
                    # if FlowStats occured from a link congestion
                    if (dpid1, port1) not in self.flow_dict:
                        self.flow_dict[(dpid1,port1)] = {} 
                    if destIP not in self.flow_dict[(dpid1,port1)]:
                        self.flow_dict[(dpid1,port1)][destIP] = []
                    self.flow_dict[(dpid1,port1)][destIP].append(sourceIP)

        if port1 != None:       
            dpid2 = self.link_up[(dpid1, port1)]
            log.info('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
            if not correlating_flow_stats:           
                # if a congestion has been previously 
                # happened (stats from previous congestions 
                # are available) and this congested link
                # is not included in the previoulsy rerouted 
                # paths, then store these FlowStats for a 
                # correlation check later
                rerouted_link_congested = True
                if self.monitor_congested_links_dict:
                    # a congestion has been previously
                    # happened, so this congestion is 
                    # a trigger for correlating the old
                    # with the current congestions
                    rerouted_link_congested = False
                    for congested_link_dict in self.monitor_congested_links_dict.itervalues():
                        if (dpid1, dpid2) in congested_link_dict['REROUTING_LINKS']:
                            rerouted_link_congested = True
                            break
                if not rerouted_link_congested:
                    log.info('Correlating Congested link: '+str(dpid1)+' - '+str(dpid2))
                    # store the flow info of this congested link
                    # copy from the flow_dict
                    #if (dpid1, port1) not in self.current_congestions_info_dict:
                    #    self.current_congestions_info_dict[(dpid1,port1)] = {}
                    self.current_congestions_info_dict[(dpid1,port1)] = {}
                    self.current_congestions_info_dict[(dpid1,port1)] = copy.deepcopy(self.flow_dict[(dpid1,port1)])

                # gather aggregate stats
                # for the congestion as 
                # it has to be mitigated 
                flow_dests = self.flow_dict[(dpid1,port1)].keys()
                self.congestedLinkMeasuredDests[(dpid1, port1)] = []
                for dest in flow_dests:
                    self.aggr_time_measure[(dpid1, port1, dest)] = self.seconds
                    Timer(self.aggr_stats_measure_interval, self.send_dest_aggregate_flow_request, args=[dpid1, port1, dest], recurring=True)
                    self.congestedLinkMeasuredDests[(dpid1, port1)].append(dest)
            else:
                # if the last FlowStat for correlating
                # the congested links is received, then 
                # check if returning sources appear
                if last_correlating_flow_stats:
                    self.increase_source_ranking()
        else:
            log.info('*************************************')        
    
    def reformIP(self, value):
        ip_str = str(value)
        if '/' in ip_str:
            ip_str = ip_str.split('/')[0]
        ip = str(ip_str)
        return ip

    def send_dest_aggregate_flow_request(self, dpid, port, dest):        
        if self.aggr_time_measure[(dpid, port, dest)] >= 0: 
            log.info("DPID, PORT, DEST: "+str(dpid)+" , "+str(port)+" , "+str(dest)+" TIME: "+str(self.aggr_time_measure[(dpid, port, dest)]))
            self.aggr_time_measure[(dpid, port, dest)] = self.aggr_time_measure[(dpid, port, dest)] - self.aggr_stats_measure_interval
            self.dest_aggregate_flow_message(dpid, port, dest)
        else:
            self.congestedLinkMeasuredDests[(dpid, port)].remove(dest)
            if not self.congestedLinkMeasuredDests[(dpid, port)]:
                del self.congestedLinkMeasuredDests[(dpid, port)]
                log.info("==================== @@@@@@@@@@@@@@ =======================")
                log.info("DPID, PORT: "+str(dpid)+" , "+str(port)+" TIME: "+str(self.aggr_time_measure[(dpid, port, dest)]))
                log.info("self.concurrent_congested_links: "+str(self.concurrent_congested_links))
                for links_group in sorted(self.concurrent_congested_links):
                    log.info("link group: " + str(links_group))
                    if (dpid, port) in self.concurrent_congested_links[links_group]:
                        self.concurrent_congested_links[links_group].remove((dpid,port))
                        break   
                if not self.concurrent_congested_links[links_group]:
                    del self.concurrent_congested_links[links_group]
                    # if the aggregate stats were all gathered
                    # after the concurrent congestions interval expired
                    if self.concurrent_congested_links_interval_expired:
                        log.info('Concurrent congestions timer expired')
                        log.info('All aggregate stats gathered')
                        self.concurrent_congested_links_interval_expired = False
                        # Check if old stats for correlation exist
                        # i.e. a congestion has already happened
                        if self.monitor_congested_links_dict:
                            log.info('Old stats exist - Request correlating stats')
                            # If yes, then request stats from rerouting 
                            # paths to check for correlation with the
                            # congested links (increase ranking)
                            self.request_correlating_flow_stats()
                        else:
                            log.info('Old stats dont exist - Calculate histograms')
                            # if old stats don't exist, then 
                            # a correlation is not possible.
                            # So proceed with calculating the
                            # bandwidth processing of the destinations
                            # passing through the congested links
                            self.calculate_histograms()
            return False        

    def dest_aggregate_flow_message(self, dpid, port, dest):
        dest_match = of.ofp_match()
        # IP type
        dest_match.dl_type = 0x0800
        # nw_proto = {'TCP':6,'UDP':17,'ICMP':1}
        # TCP traffic
        dest_match.nw_proto = 6
        # Destination IP
        dest_match.nw_dst = dest 
        stats_request_msg = of.ofp_stats_request(body=of.ofp_aggregate_stats_request(match = dest_match, out_port = port))
        stats_msg_xid = stats_request_msg.xid
        # Stats XID to link dict
        self.xidToLink[stats_msg_xid] = (dpid, port, dest)
        core.openflow.sendToDPID(dpid, stats_request_msg)

    def check_aggr_stats_finished(self, links_group):
        log.info('check_aggr_stats_finished')
        # WRITE TO FILE
        #self.defence_init_file.write('CONCURRENT_CONGESTIONS_INTERVAL_EXPIRED: '+str(time.time())+'\n')
        #self.defence_init_file.flush()
        # If the aggregate stats were all gathered before 
        # the concurrent congestions interval expired.
        if links_group not in self.concurrent_congested_links:
            log.info('Aggr stats gathered before timer expires')
            # Check if old stats for correlation exist
            # i.e. a congestion has already happened
            if self.monitor_congested_links_dict:
                log.info('Old stats exist - Request correlating stats')
                # If yes, then request stats from rerouting 
                # paths to check for correlation with the
                # congested links (increase ranking)
                self.request_correlating_flow_stats()
            else:
                log.info('Old stats dont exist - Calculate histograms')
                # if old stats don't exist, then 
                # a correlation is not possible.
                # So proceed with calculating the
                # bandwidth processing of the destinations
                # passing through the congested links
                self.calculate_histograms()
        else:
            log.info('Remaining aggregate stats to be gathered')            
            # There are still some aggregate stats
            # to be gathered, so the ranking check
            # or the calculation of the bandwidth of 
            # the destinations passing through the 
            # congested links will be done after all 
            # of them have been gathered
            self.concurrent_congested_links_interval_expired = True


    def _handle_AggregateFlowStatsReceived(self, event):
        log.info("_handle_AggregateFlowStatsReceived")
        first_time = False
        stats = event.stats
        dpid, port, dest = self.xidToLink[event.ofp.xid]
        log.info("=====================================")
        log.info("link DPID: "+str(dpid)+" - Port: "+str(port)+" - Dest: "+str(dest))
        log.info("=====================================")
        # remove entry after using it
        del self.xidToLink[event.ofp.xid]
        
        if (dpid, port) not in self.bw:
            self.bw[(dpid,port)] = {}
        #if (dpid, port) not in self.flows:
        #    self.flows[(dpid,port)] = {}
            
        if (dpid, port, dest) not in self.aggr_dest_stats:
            first_time = True
            self.dest_bw[(dpid, port, dest)] = []
            #self.dest_flows[(dpid, port, dest)] = []
            self.bw[(dpid,port)][dest] = 0
            #self.flows[(dpid,port)][dest] = 0
            self.aggr_dest_stats[(dpid, port, dest)] = {'packet_count':0,'byte_count':0}

        # avoid having negative bandwidth
        # when the counters are stragely reset
        current_packets = stats.packet_count - self.aggr_dest_stats[(dpid, port, dest)]['packet_count']
        current_bytes = stats.byte_count - self.aggr_dest_stats[(dpid, port, dest)]['byte_count']
        current_flows = stats.flow_count
        if current_bytes < 0:
            log.info("COUNTERS RESET!!!!!!!!!!")
            self.aggr_dest_stats[(dpid, port, dest)]['packet_count'] = stats.packet_count
            self.aggr_dest_stats[(dpid, port, dest)]['byte_count'] = stats.byte_count
            self.aggr_time_measure[(dpid, port, dest)] = self.aggr_time_measure[(dpid, port, dest)] + self.aggr_stats_measure_interval
        else: 
            #current_bandwidth = (current_bytes * 8) / float(current_flows) 
            current_bandwidth = current_bytes * 8

            self.aggr_dest_stats[(dpid, port, dest)]['packet_count'] = stats.packet_count
            self.aggr_dest_stats[(dpid, port, dest)]['byte_count'] = stats.byte_count

            log.info("Aggregate stats - packet count: "+str(stats.packet_count))
            log.info("Aggregate stats - byte count: "+str(stats.byte_count))
            #log.info("Aggregate stats - flow count: "+str(stats.flow_count))  

            if first_time == False:
                self.dest_bw[(dpid, port, dest)].append(current_bandwidth)
                #self.dest_flows[(dpid, port, dest)].append(current_flows)
                if self.aggr_time_measure[(dpid, port, dest)] < 0.0:
                    log.info("self.aggr_time_measure[(dpid, port)]: "+str(self.aggr_time_measure[(dpid, port, dest)]))
                    self.bw[(dpid,port)][dest] = sum([self.estimation_weights[i] * self.dest_bw[(dpid, port, dest)][i] for i in range(len(self.dest_bw[(dpid, port, dest)]))])
                    #self.flows[(dpid,port)][dest] = sum([self.estimation_weights[i] * self.dest_flows[(dpid, port, dest)][i] for i in range(len(self.dest_flows[(dpid, port, dest)]))])
            
                log.info("Aggregate stats - current packets: "+str(current_packets))       
                log.info("Aggregate stats - current bytes: "+str(current_bytes))  
                log.info("Aggregate stats - current bandwidth (in bps): "+str(current_bandwidth))
                #log.info("Aggregate stats - current flows: "+str(current_flows))  
                if self.aggr_time_measure[(dpid, port, dest)] < 0.0:
                    log.info("Aggregate stats - average bandwidth (in bps): "+str(self.bw[(dpid,port)][dest]))
                    #log.info("Aggregate stats - average flows: "+str(self.flows[(dpid,port)][dest]))

    def calculate_histograms(self):
        msg = {}
        msg['REROUTING'] = {}
        log.info("=================================")
        log.info("           Bandwidth             ")
        log.info("self.bw: "+str(self.bw))
        for link, dest_bw_dict in self.bw.iteritems():
            log.info("link (dpid,port): "+str(link)+" destIP - BW dict: "+str(dest_bw_dict))
            # sort the dest_bw_dict regarding the highest BW per destination
            # returns sorted list of (destIP, BW) tuples 
            #sorted_dest_bw_list =  sorted(dest_bw_dict.items(), key=lambda x:x[1], reverse=True)
            #log.info("link (dpid,port): "+str(link)+" sorted destIP - BW dict: "+str(sorted_dest_bw_list))
            
            rerouting_bw_threshold = self.rerouting_bw_threshold_percent * self.link_nominal_bw[tuple(sorted((link[0],self.link_up[link])))]
            log.info("rerouting_bw_threshold: "+str(rerouting_bw_threshold))

            # get the dpid1 and dpid2 of the congested link
            dpid1 = link[0] 
            dpid2 = self.link_up[link]

            msg['REROUTING'][(dpid1, dpid2)] = {}
            msg['REROUTING'][(dpid1, dpid2)]['dstIP'] = []
            msg['REROUTING'][(dpid1, dpid2)]['bw'] = []
            msg['REROUTING'][(dpid1, dpid2)]['srcIPlist'] = []

            # classify the dest_bw_dict with respect to destinations
            # that occupy homogeneous bandwidth at the congested link
            # returns sorted list of (destIP, BW) tuples 
            sorted_dest_bw_list = self.homogeneous_bandwidth_classification(dest_bw_dict)
            for dest_bw_tuple in sorted_dest_bw_list:
                dest, bw = dest_bw_tuple
                # reroute the Destinations that fit the rerouting threshold 
                # If a destination does not fit, do not check the next Dest 
                # to avoid rerouting a potential Target Dest
                if bw <= rerouting_bw_threshold:
                    msg['REROUTING'][(dpid1, dpid2)]['dstIP'].append(dest) 
                    msg['REROUTING'][(dpid1, dpid2)]['bw'].append(bw) 
                    msg['REROUTING'][(dpid1, dpid2)]['srcIPlist'].append(self.flow_dict[link][dest])        
                    rerouting_bw_threshold = rerouting_bw_threshold - bw
                else:
                    break
            # reroute the 2nd destination in order if none of the destinations
            # are selected for rerouting. This mitigates the congestion when 
            # only two destinations carry the total aggregate BW with
            # individual BW value that exceeds the rerouting_bw_threshold 
            if not msg['REROUTING'][(dpid1, dpid2)]['dstIP']:
                dest, bw = sorted_dest_bw_list[1]                
                msg['REROUTING'][(dpid1, dpid2)]['dstIP'].append(dest) 
                msg['REROUTING'][(dpid1, dpid2)]['bw'].append(bw) 
                msg['REROUTING'][(dpid1, dpid2)]['srcIPlist'].append(self.flow_dict[link][dest])        

            # flush the gathered aggregate stats for the 
            # destinations passing through the congested link
            for dest in dest_bw_dict.iterkeys():
                link_dest_tuple = link + (dest,)
                del self.aggr_dest_stats[link_dest_tuple]                
            # flush the reported parameters regarding the congested link:
            # self.flow_dict, self.dest_bw, self.bw
            del self.flow_dict[link]
            log.info('Modified self.flow_dict: '+str(self.flow_dict))
            dests = self.bw[link].keys()
            for dest in dests:
                del self.dest_bw[link+(dest,)]
            log.info('Modified self.dest_bw: '+str(self.dest_bw))  

        Timer(5.0, self.flush_target_link, args=self.bw.keys())
  
        self.bw = {}
        log.info('Modified self.bw: '+str(self.bw))
        log.info("#################################################")    
 
        # send also the current traffic matrix (available BW)
        msg['TRAFFIC_MATRIX'] = {}
        for link, dpid2 in self.link_up.iteritems():
            log.info("QQQQ: "+str(link)+" WWWW: "+str(tuple(sorted((link[0],dpid2))))+" used_bw[link]: "+str(self.link_used_bw[link]))
            log.info("self.link_nominal_bw[tuple(sorted((link[0],dpid2)))] - self.link_used_bw[link]: "+str(self.link_nominal_bw[tuple(sorted((link[0],dpid2)))] - self.link_used_bw[link])) 
            msg['TRAFFIC_MATRIX'][(link[0], dpid2)] = self.link_nominal_bw[tuple(sorted((link[0],dpid2)))] - self.link_used_bw[link]
        # and finally send the source ranking dict
        # in order to rate-limit the traffic if 
        # ranking exceeded a predefined threshold
        msg['RANKING'] = copy.deepcopy(self.source_ranking)
        # WRITE TO FILE
        #self.defence_init_file.write('CONGESTION_REPORTED: '+str(time.time())+'\n')
        #self.defence_init_file.flush()
        # raise a LinkCongestion Event, and send the message
        log.info("LinkCongestion Event raised") 
        self.raiseEvent(LinkCongestion, msg)


    def homogeneous_bandwidth_classification(self,dest_bw_dict):
        log.info('homogeneous_bandwidth_classification')
        # get all destiantions
        dests = dest_bw_dict.keys()
        # calculate all their pair combinations
        combs = list(combinations(dests,2))
        # find the pair with with min absolute
        # difference in terms of their BW
        min = float('inf')
        for comb in combs:
            if abs(dest_bw_dict[comb[0]] - dest_bw_dict[comb[1]]) < min:
                min = abs(dest_bw_dict[comb[0]] - dest_bw_dict[comb[1]])
                min_comb = comb
        log.info('min: '+str(min))
        log.info('min_comb: '+str(min_comb))
        # list with the sorted destinations, index 0 -> Dest sorted 1st
        # index 1 -> Dest sorted 2nd etc. 
        rerouting_order = []
        # put in the list the dests of the
        # pair with the min BW difference
        if dest_bw_dict[min_comb[0]] > dest_bw_dict[min_comb[1]]:
            rerouting_order.append(min_comb[0])
            rerouting_order.append(min_comb[1])
        else:
            rerouting_order.append(min_comb[1])
            rerouting_order.append(min_comb[0])
        log.info('dests: '+str(dests))
        # remove the dests that were already sorted from 
        # the list of the remaining dests to be sorted
        dests.remove(rerouting_order[0])
        dests.remove(rerouting_order[1])
        log.info('dests: '+str(dests))
        # until all dests are sorted, find the dest with 
        # the min BW difference from the last sorted one
        while len(rerouting_order) != len(dest_bw_dict):
            reference = rerouting_order[-1]
            min = float('inf')
            for dest in dests:
                if abs(dest_bw_dict[reference] - dest_bw_dict[dest]) < min:
                    min = abs(dest_bw_dict[reference] - dest_bw_dict[dest])
                    min_dest = dest
            rerouting_order.append(min_dest)
            dests.remove(min_dest)
        log.info('rerouting_order: '+str(rerouting_order))
        # sort the dest_bw_dict of a target link with 
        # respect to the rerouting_order list
        # It returns a sorted list of (DestIP,BW) tuples
        sorted_dest_bw_dict = sorted(dest_bw_dict.items(),key=lambda x: rerouting_order.index(x[0]))
        log.info('sorted_dest_bw_dict: '+str(sorted_dest_bw_dict))      
        return sorted_dest_bw_dict


    def flush_target_link(self, *links_list):
        # flush target_links dict used
        # to group the concurrent congestions
        for link in links_list:
            del self.target_links[link]
        log.info('Target links flushed !!!')


    def request_correlating_flow_stats(self):
        log.info('request_correlating_flow_stats')
        for congested_link in self.monitor_congested_links_dict.iterkeys():
            for (entry_dpid, entry_port) in self.monitor_congested_links_dict[congested_link]['SW'].iterkeys(): 
                stats_request_msg = of.ofp_stats_request(body=of.ofp_flow_stats_request(out_port = entry_port))
                stats_msg_xid = stats_request_msg.xid
                self.correlatingXids.append(stats_msg_xid)
                core.openflow.sendToDPID(entry_dpid, stats_request_msg)
                log.info('Congested link: '+str(congested_link)+' - Requesting FlowStats from: DPID: ' +str(entry_dpid)+' - Port: '+str(entry_port))


    def increase_source_ranking(self):
        log.info('increase_source_ranking')
        # store the sources that dissapeared
        # from the rerouting paths 
        suspicious_sources = []
        # At first, check that some sources maybe 
        # dissapeared from the rerouted paths
        for congested_link_dict in self.monitor_congested_links_dict.itervalues():
            for rerouted_entry_link in congested_link_dict['SW'].iterkeys():
                rerouted_dsts_list = congested_link_dict['SW'][rerouted_entry_link]['dstIP']
                rerouted_srciplist_list = congested_link_dict['SW'][rerouted_entry_link]['srcIPlist']
                # for each rerouted destination
                for i, dst in enumerate(rerouted_dsts_list):
                    # check if current flows to previously 
                    # rerouted destinations exist
                    if dst not in self.correlation_dict[rerouted_entry_link].keys():
                        # if not then keep all the corresponding 
                        # srcIPs that dissapeared
                        for srcip in rerouted_srciplist_list[i]: 
                            if srcip not in suspicious_sources:
                                suspicious_sources.append(srcip)
                                log.info('Dest dissapeared')
                                log.info('Rerouted entry link: '+str(rerouted_entry_link)+' - Dest: '+str(dst)+' Src: '+str(srcip))
                    else:
                        # then check if some srcIPs have disappeared
                        current_srcip_list = self.correlation_dict[rerouted_entry_link][str(dst)]
                        rerouted_srcip_list = rerouted_srciplist_list[i]
                        for srcip in list(set(rerouted_srcip_list)-set(current_srcip_list)):
                            if srcip not in suspicious_sources:
                                suspicious_sources.append(srcip)
                                log.info('Source from this Dest just dissapeared')
                                log.info('Rerouted entry link: '+str(rerouted_entry_link)+' - Dest: '+str(dst)+' Src: '+str(srcip))
        log.info('suspicious_sources: '+str(suspicious_sources))
        marked_sources = []
        # then check if the sources that dissapeared 
        # are present in the current congested links
        for congested_link in self.current_congestions_info_dict.iterkeys():
            log.info('congested_link: '+str(congested_link))
            for srcip_list in self.current_congestions_info_dict[congested_link].itervalues():
                log.info('srcip_list: '+str(srcip_list))
                srcip_list = map(lambda x: IPAddr(x),srcip_list)
                log.info('srcip_list: '+str(srcip_list))
                log.info('list(set(srcip_list) & set(suspicious_sources)): '+str(list(set(srcip_list) & set(suspicious_sources))))
                for srcip in list(set(srcip_list) & set(suspicious_sources)): 
                    if srcip not in marked_sources:    
                        marked_sources.append(srcip)
        log.info('marked_sources: '+str(marked_sources))
        # increase the rank of the marked_sources
        for source in marked_sources:
            if source not in self.source_ranking:
                self.source_ranking[source] = 1
            else:
                self.source_ranking[source] = self.source_ranking[source] + 1
        log.info('source_ranking: '+str(self.source_ranking))                
        # delete the monitoring entries
        self.monitor_congested_links_dict = {}
        log.info('self.monitor_congested_links_dict modified: '+str(self.monitor_congested_links_dict))
        # delete the correlation dict entries
        self.correlation_dict = {}
        log.info('self.correlation_dict modified: '+str(self.correlation_dict))
        # delete the current congestions dict entries
        self.current_congestions_info_dict = {}
        log.info('self.current_congestions_info_dict modified: '+str(self.current_congestions_info_dict))
        # Then, the destinations passing through
        # the congested links are processed in terms
        # of bandwidth to execute the rerouting 
        self.calculate_histograms()

    
    def _handle_DijkstraRouting_ReroutingMonitoring(self, event):
        log.info('ReroutingMonitoring msg received')
        msg = event.msg
        # store the info about the reroutings
        #if not self.monitor_congested_links_dict:
        self.monitor_congested_links_dict = copy.deepcopy(msg)
        #else:
            #self.monitor_congested_links_dict.update(msg)
        log.info('self.monitor_congested_links_dict: '+str(self.monitor_congested_links_dict))
        
    
    def read_topo_link_capacities(self):
        mininet_full_path = os.path.join(os.getcwd(),"ext","crossfire_pox","mininet")
        linkBW_file_full_path = os.path.join(mininet_full_path,"linkBW.txt")
        linkBW_file = open(linkBW_file_full_path,'r')
        self.link_nominal_bw = {}
        for line in linkBW_file.readlines():
            list = line.split()
            # ['sw:', 's1', 'sw:', 's2', 'bw:', '1000.0']
            # sorted tuples as keys
            # for the links_bw dict
            # e.g. (3,5), (1,2), (5,6)
            # bandwidth (bw) in Mbps
            switch1 = int(list[1].split('s')[1])
            switch2 = int(list[3].split('s')[1])
            # bandwidth (bw) in bps
            bw = float(list[5]) * 1000000.0
            self.link_nominal_bw[(switch1, switch2)] = bw
        linkBW_file.close()
        log.info("self.link_nominal_bw:"+str(self.link_nominal_bw))

    def read_target_links(self):
        # Every 15.0 secs
        # check if there is a change
        # in the calculated target links
        Timer(10.0, self.read_target_links)
        mininet_full_path = os.path.join(os.getcwd(),"ext","crossfire_pox","mininet")
        target_links_file_full_path = os.path.join(mininet_full_path,"target_links.txt")
        # if file exists
        if os.path.isfile(target_links_file_full_path):
            target_links_file = open(target_links_file_full_path,'r')
            target_links_read = []
            for line in target_links_file.readlines():
                list = line.rstrip().split()
                target_link = tuple(map(lambda x: int(x.split('.')[-1]),list))
                target_links_read.append(target_link)
            if self.expected_target_links != target_links_read:
                self.expected_target_links = target_links_read
            target_links_file.close()


    def open_results_files(self):
        # /home/mininet/pox is the CWD
        results_full_path = os.path.join(os.getcwd(),"ext","crossfire_pox","results")
        reaction_times_full_path = os.path.join(results_full_path,"reaction_times")
        attacker_reaction_times_full_path = os.path.join(reaction_times_full_path,"attacker")
        defender_reaction_times_full_path = os.path.join(reaction_times_full_path,"defender")
        defence_init_file_path = os.path.join(defender_reaction_times_full_path,"defence_init.txt")
        attack_complete_file_path = os.path.join(attacker_reaction_times_full_path,"attack_complete.txt")
        self.defence_init_file = open(defence_init_file_path,"w")
        self.attack_complete_file = open(attack_complete_file_path,"w")


def launch():
    log.info("Loading Link Monitoring module")
    core.registerNew(LinkMonitoring)

